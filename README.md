# futureglimpses.earth website

    npm install
    npm start

[Markdown reference](https://markdown-it.github.io/)


## Deployment configuration

* [Pipeline](https://gitlab.com/sjuthorup/futureglimpses/pipelines)
* [AWS](./aws-setup.md)
* [Cloudflare](./cloudflare-setup.md)
