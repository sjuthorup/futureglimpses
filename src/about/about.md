---
layout: page-en
permalink: "/about/"
---
<i>(English below)</i>

### Glimt af vores fremtid
Her får du positive billeder af fremtiden. Når vi lykkes med den grønne omstilling, er naturen under genopretning, energi fremstilles fossilfrit, og menneskenes ressourcebelastning er kommet ned på et bæredygtigt niveau.

Hvert fremtidsglimt er en hel historie i komprimeret form, som viser et øjeblik fra en dagligdag eller et element i hvordan samfundet fungerer. Årstallet er 2043. Fortælleren er en ung studerende med venner, studiejob og fremtidsdrømme. Klik på "Catch a glimpse" herover for at se et nyt fremtidsglimt.

Fremtidsglimtene kan læses i små portioner og inspirere til refleksion og samtale om den grønne omstilling. Find de fremtidsglimt, som gør dig optimistisk og giver dig lyst til at arbejde for den grønne omstilling. Prøv at læse nogle fremtidsglimt højt for venner og familie, og sæt gang i en spændende snak.

Fremtidsglimtene udkom som billedblog fra oktober 2019 til juli 2021. Der er 288 fremtidsglimt ialt. 
To samlinger af fremtidsglimtene er udgivet <a href= "/books/"> som bøger.</a> 
Fremtidsglimtene findes også på instagramprofilen <a href="https://www.instagram.com/futureglimpses/" target="_blank">@futureglimpses</a> (dog kun med de engelske tekster).

Skaberen af Fremtidsglimt er Sju G. Thorup, ledelseskonsulent og aktiv i den danske klimabevægelse. <a href="https://sju.xpqf.com/profil/" target="_blank">Kontaktdata</a> her.


![](Sju-draws-chickens.jpg)

### Glimpses of our future

Here you can find inspirational images of the future we will have -- when we succeed with the green transition. Nature is being rewilded, energy production is fossil free, and humanity’s resource demand has been lowered to a sustainable level.

Each 'future glimpse' is a whole story compressed into one sentence and an image. Take your time reading them, and reflect on how each specific glimpse can enlighten your own vision about the future.

The narrator is a young student with friends, extracurricular activities and dreams of her future. The time is 20-25 years from now, let's say 2043. Click "Catch a glimpse" above to see another future glimpse.

The future glimpses were published as a blog from October 2019 to July 2021.
There are 288 in total. Two selections are published <a href= "/books/"> as books</a>  (only in Danish).
You can also find the future glimpses at the instagram profile <a href="https://www.instagram.com/futureglimpses/" target="_blank">@futureglimpses.</a>

Creator of 'Glimpses of Our Future' is Sju G. Thorup, management consultant and active member of the Danish climate movement. Contact info <a href="https://sju.xpqf.com/en/profile/" target="_blank">here</a>.
