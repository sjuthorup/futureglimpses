---
layout: page-en
permalink: "/books/"
---

<p>
  <img style="display: inline; height: 1em" src="./flag-us.png"/>
  <img style="display: inline; height: 1em" src="./point-right.png"/>
  <i>No books in English yet, sorry.</i>
</p>

## Bøger

Danske læsere kan nu købe to udvalg af fremtidsglimt i små bøger.

Fremtidsglimtene kan læses i små portioner, som kan inspirere til refleksion og samtale om den grønne omstilling. Prøv at læse nogle fremtidsglimt højt for venner og familie, og sæt gang i en spændende snak.
Klik på "Catch a glimpse" foroven for at se et nyt fremtidsglimt.

Bøgerne er velegnet til værtindegave, mandelgave og ”coffee-table book”. De kan også give inspiration til studieprojekter om den grønne omstilling.

Spørg efter bøgerne hos din lokale boghandler, eller bestil dem online.

<b>Fremtidsglimt 1. samling</b> -
Find fremtidsglimt om bl.a. det faldende olieforbrug, 3D teknologi, byer fyldt af grønne planter, dele-kæledyr, ligeløn og flekstid, lykkeindekset, miljørigtigt fyrværkeri, og hvordan de unge i år 2043 vil se på vores nutid.
Bogen indeholder 55 fremtidsglimt.
Vejledende pris 79 kr -- [Online-køb fx her.](https://www.williamdam.dk/fremtidsglimt-1-samling__2501538)

<b>Fremtidsglimt 2. samling</b> -
Her findes nye fremtidsglimt i temaer som Sundhed, Den grønne omstilling og Cirkulær økonomi, og man lærer hovedpersonen bedre at kende.
Se fremtidsglimt om bl.a. advokater for øko-systemerne, hvaler der giver råd om havmiljøet, koncert på kraftværket, genskabte mammutter, byer hvor alt er i gå-afstand, metaludvinding med planter, og om de mange jobs som blev forvandlet i den grønne omstilling.
Bogen indeholder 94 fremtidsglimt.
Vejledende pris 99 kr --
[Online-køb fx her.](https://www.williamdam.dk/fremtidsglimt-2-samling__2501539)

![](both-books.jpg)
