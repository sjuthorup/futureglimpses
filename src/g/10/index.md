---
date: 2019-12-02
layout: post-en
permalink: "/g/10/"
---
## My 10-year old cousin came home from her history lesson, unbelieving: How could people just burn all those fossil fuels without thinking of the consequences?

{% prevButton page collections %}
![](10-fossil-consequences.jpg)
{% nextButton page collections %}

### *Min 10-årige kusine kom hjem fra sin historietime og sagde vantro: Hvordan kunne folk bare brænde alle de fossile brændsler uden at tænke over konsekvenserne?*
