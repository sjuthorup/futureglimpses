---
date: 2020-05-06
layout: post-en
permalink: "/g/100/"
---
## Most clothes are made-on-demand, so they fit, and waste is minimal

{% prevButton page collections %}
![](100-custom-clothes.jpg)
{% nextButton page collections %}

### *Det meste tøj produceres stykvis efter behov, så det passer, og spildet er minimalt*
