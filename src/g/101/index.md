---
date: 2020-05-09
layout: post-en
permalink: "/g/101/"
---
## Consensus seems to form in the UN to implement a global tax on financial transactions that will fund climate resilience project in vulnerable countries

{% prevButton page collections %}
![](101-UN-tobin-tax.jpg)
{% nextButton page collections %}

### *Der ser ud til at kunne samles enighed i FN om en global skat på finansielle transaktioner, som vil finansiere klimatilpasningsprojekter i udsatte lande*
