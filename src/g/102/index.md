---
date: 2020-05-10
layout: post-en
permalink: "/g/102/"
---
## To counter market dominance, companies with a turnover of more than 15 billion euros must be split up

{% prevButton page collections %}
![](102-anti-monopoly.jpg)
{% nextButton page collections %}

### *For at modvirke markedsdominans skal virksomheder med mere end 15 milliarder euro i omsætning splittes op*
