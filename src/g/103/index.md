---
date: 2020-05-12
layout: post-en
permalink: "/g/103/"
---
## The rule that shares must be owned for at least 2 years has eliminated stock market speculation, and company boards take more far-sighted decisions

{% prevButton page collections %}
![](103-long-horizon.jpg)
{% nextButton page collections %}

### *Reglen om at man skal eje aktier i mindst 2 år har fjernet spekulation på aktiemarkederne, og bestyrelser træffer mere langsigtede beslutninger*
