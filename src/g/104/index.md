---
date: 2020-05-13
layout: post-en
permalink: "/g/104/"
---
## My mom’s research team assists the refitting of the Nordstream 2 gas pipeline to contain windpower generated hydrogen

{% prevButton page collections %}
![](104-refit-nordstream.jpg)
{% nextButton page collections %}

### *Min mors forskergruppe hjælper med omstillingen af Nordstream 2 naturgas-rørledningen til at indeholde vindmølle-produceret brint*
