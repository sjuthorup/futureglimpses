---
date: 2020-05-15
layout: post-en
permalink: "/g/105/"
---
## At our insect trip today we found 12 butterfly species, including one that has returned to this area after 50 years of absence

{% prevButton page collections %}
![](105-butterfly-returned.jpg)
{% nextButton page collections %}

### *På vores insekt-tur idag fandt vi 12 arter af sommerfugle, heraf en som er kommet tilbage til vores område efter 50 års fravær*
