---
date: 2020-05-17
layout: post-en
permalink: "/g/106/"
---
## Since the implementation of the international ban on sales of military equipment, no armed conflict has started anywhere in the world

{% prevButton page collections %}
![](106-no-conflicts.jpg)
{% nextButton page collections %}

### *Siden det verdensomspændende forbud mod salg af militærudstyr er der ikke startet væbnet konflikt noget sted i verden*
