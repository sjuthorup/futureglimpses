---
date: 2020-05-19
layout: post-en
permalink: "/g/107/"
---
## When my young cousin sleeps over, our bedtime story is looking at the Earth from the public satellite feeds

{% prevButton page collections %}
![](107-cousin-night.jpg)
{% nextButton page collections %}

### *Når min lille kusine overnatter her, er det vores godnathistorie at kigge på Jorden fra de offentlige satellit-feeds*
