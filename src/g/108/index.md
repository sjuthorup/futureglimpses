---
date: 2020-05-20
layout: post-en
permalink: "/g/108/"
---
## Treatments using bacteriophages have practically replaced antibiotics

{% prevButton page collections %}
![](108-bacteriophages.jpg)
{% nextButton page collections %}

### *Behandling med bakteriofager har næsten helt erstattet antibiotika*
