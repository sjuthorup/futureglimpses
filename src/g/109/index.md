---
date: 2020-05-22
layout: post-en
permalink: "/g/109/"
---
## Psychiatry is branching into disciplines according to the relevant biochemical test results

{% prevButton page collections %}
![](109-psychiatry.jpg)
{% nextButton page collections %}

### *Psykiatrien etablerer separate fagområder omkring de relevante biokemiske prøveresultater*
