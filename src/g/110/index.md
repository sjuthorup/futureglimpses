---
date: 2020-05-24
layout: post-en
permalink: "/g/110/"
---
## Medication is produced specifically for the individual patient to minimize side effects and waste

{% prevButton page collections %}
![](110-personalized-medicine.jpg)
{% nextButton page collections %}

### *Medicin fremstilles specifikt til den enkelte patient, så man minimerer både spild og bivirkninger*
