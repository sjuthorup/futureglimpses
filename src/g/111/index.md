---
date: 2020-05-26
layout: post-en
permalink: "/g/111/"
---
## We have full real-time transparency into the detailed EU budget and accounting, and graft and fraud are minimal

{% prevButton page collections %}
![](111-EU-funds.jpg)
{% nextButton page collections %}

### *Vi har fuld og øjeblikkelig synlighed af det detaljerede budget og regnskab i EU, og der er meget lidt bestikkelse og snyd*
