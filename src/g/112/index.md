---
date: 2020-05-28
layout: post-en
permalink: "/g/112/"
---
## EU’s import tariffs based on countries’ democracy indexes have helped improve governance and human rights in many places

{% prevButton page collections %}
![](112-import-tariff-democracy.jpg)
{% nextButton page collections %}

### *EUs told, som afspejler landenes demokrati-indeks, har hjulpet til at forbedre regeringsførelse og menneskerettigheder mange steder*
