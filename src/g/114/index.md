---
date: 2020-05-31  
layout: post-en
permalink: "/g/114/"
---
## Belgium’s world-class spoken language translation technologies benefit the whole world

{% prevButton page collections %}
![](114-belgium-translation.jpg)
{% nextButton page collections %}

### *Belgiens avanceredee teknologier til talesprogsoversættelse er til nytte for hele verden*
