---
date: 2020-06-02
layout: post-en
permalink: "/g/115/"
---
## Germany’s wide-ranging expertise in workload flexibility schemes inspires countries all over Europe

{% prevButton page collections %}
![](115-germany-jobsharing.jpg)
{% nextButton page collections %}

### *Tysklands verdensførende modeller for job-deling og fleksibel arbejdstid inspirerer lande i hele Europa*
