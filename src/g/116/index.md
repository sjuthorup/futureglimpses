---
date: 2020-06-04
layout: post-en
permalink: "/g/116/"
---
## Estonia’s privacy-secure public service automation helps save tons of paper each year all over the world

{% prevButton page collections %}
![](116-estonia-IT.jpg)
{% nextButton page collections %}

### *Estlands datasikre automatiseringsløsninger til offentlig service bidrager til at spare tonsvis af papir hvert år i hele verden*
