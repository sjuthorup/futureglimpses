---
date: 2020-06-08
layout: post-en
permalink: "/g/118/"
---
## In addition to its windpower expertise, Denmark is the world leader in small co-working robots

{% prevButton page collections %}
![](118-denmark-cobots.jpg)
{% nextButton page collections %}

### *Ud over sin vindkraftekspertise er Danmark førende indenfor små samarbejdsrobotter*
