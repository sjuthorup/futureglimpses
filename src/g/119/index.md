---
date: 2020-06-09
layout: post-en
permalink: "/g/119/"
---
## Lithuanian expertise in laser technology is used worldwide for precision cancer surgery with minimal side effects

{% prevButton page collections %}
![](119-lithuania-laser.jpg)
{% nextButton page collections %}

### *Litauisk ekspertise i laserteknologi bruges over hele verden til kræftkirurgi, der er præcis og stort set bivirkningsfri*
