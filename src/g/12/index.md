---
date: 2019-12-05
layout: post-en
permalink: "/g/12/"
---
## Maersk announced that their fleet of tankers is now all-electric

{% prevButton page collections %}
![](12-maersk-electric.jpg)
{% nextButton page collections %}

### *Maersk udsendte pressemeddelelse om at hele deres flåde af tankskibe nu er elektrisk*
