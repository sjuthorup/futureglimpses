---
date: 2020-06-10
layout: post-en
permalink: "/g/120/"
---
## Companies in the Netherlands are global leaders in dike construction, involved in projects around the world

{% prevButton page collections %}
![](120-netherlands-dikes.jpg)
{% nextButton page collections %}

### *Hollandske virksomheder er verdensledende i dige-konstruktion og er involveret i projekter over hele kloden*
