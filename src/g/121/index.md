---
date: 2020-06-12
layout: post-en
permalink: "/g/121/"
---
## Enjoying my day off, listening to blackbird song, tits’ tweets, bee hums, grasshoppers chirping, a woodpecker drumming, and leaves rustling in the breeze

{% prevButton page collections %}
![](121-enjoy-nature.jpg)
{% nextButton page collections %}

### *Nyder min fridag og lytter til mejsekvidder, solsortesang, biers summen, græshoppers klik, en spættes hakken, og bladene der vifter i brisen*
