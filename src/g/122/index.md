---
date: 2020-06-18
layout: post-en
permalink: "/g/122/"
---
## New technology and infrastructure construction must pass a risks, harm & waste review by a multi-disciplinary board

{% prevButton page collections %}
![](122-harm-review.jpg)
{% nextButton page collections %}

### *Ny teknologi og infrastruktur skal have deres risiko, ulemper og affalds-aspekter gennemgået af et tværfagligt udvalg*
