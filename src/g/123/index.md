---
date: 2020-06-20
layout: post-en
permalink: "/g/123/"
---
## Countries and the EU have appointed advocates to represent the interests of ecosystems

{% prevButton page collections %}
![](123-eco-advocate.jpg)
{% nextButton page collections %}

### *I de enkelte lande og på EU-niveau er der udpeget advokater som repræsenterer økosystemernes interesser*
