---
date: 2020-06-23
layout: post-en
permalink: "/g/124/"
---
## Since we realized the health effects of proximity to green plants, cities have been filled with trees, windowboxes and flowerbeds

{% prevButton page collections %}
![](124-plants-city.jpg)
{% nextButton page collections %}

### *Efter det blev klart, hvor sundt det er at være i nærheden af grønne planter, er byerne blevet fyldt af træer, altankasser og bede*
