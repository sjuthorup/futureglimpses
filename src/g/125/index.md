---
date: 2020-06-25
layout: post-en
permalink: "/g/125/"
---
## The Carbon Control Agency was granted access to several more data sources, so now we can control and investigate the CO2 flows even better

{% prevButton page collections %}
![](125-carbon-data.jpg)
{% nextButton page collections %}

### *Kulstofstyrelsen fik godkendt vores ansøgning om adgang til flere datakilder, så nu kan vi kontrollere og efterforske CO2-strømmene endnu bedre*
