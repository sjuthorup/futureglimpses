---
date: 2020-06-27
layout: post-en
permalink: "/g/126/"
---
## We have an excellent delivery of fresh produce due to the integrated logistics

{% prevButton page collections %}
![](126-leeks-logistics.jpg)
{% nextButton page collections %}

### *Vi får fuldkommen frisk frugt og grønt på grund af de integrerede leverancekæder*
