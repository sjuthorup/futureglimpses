---
date: 2020-07-02
layout: post-en
permalink: "/g/127/"
---
## EU knowhow benefits the world. Finland is a role model in sustainable forestation and wood construction technologies

{% prevButton page collections %}
![](127-finland-woodtech.jpg)
{% nextButton page collections %}

### *Verden har nytte af EUs ekspertise. Finland er rollemodel indenfor bæredygtigt skovbrug og trækonstruktioner*
