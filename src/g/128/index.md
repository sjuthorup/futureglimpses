---
date: 2020-07-10
layout: post-en
permalink: "/g/128/"
---
## Austrian and Swedish companies produce globally renowned excellent equipment for pumped hydropower energy storage

{% prevButton page collections %}
![](128-hydropower.jpg)
{% nextButton page collections %}

### *Virksomheder fra Østrig og Sverige producerer udstyr i verdensklasse til pumpekraft-baseret energilagring*
