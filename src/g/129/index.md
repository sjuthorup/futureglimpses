---
date: 2020-07-12
layout: post-en
permalink: "/g/129/"
---
## The Czech Republic is the world’s largest exporter of light-rail systems design

{% prevButton page collections %}
![](129-czech-lightrail.jpg)
{% nextButton page collections %}

### *Tjekkiet er verdens største eksportør af konstruktionsviden for letbanesystemer*
