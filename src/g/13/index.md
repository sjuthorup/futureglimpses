---
date: 2019-12-06
layout: post-en
permalink: "/g/13/"
---
## We watched a great documentary about the old lady who instituted the climate strikes when she was just 15

{% prevButton page collections %}
![](13-lady-school-strike.jpg)
{% nextButton page collections %}

### *Vi så en mægtig god dokumentarfilm om den ældre dame, der startede klimastrejkerne dengang hun kun var 15 år*
