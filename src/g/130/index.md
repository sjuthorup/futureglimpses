---
date: 2020-07-15
layout: post-en
permalink: "/g/130/"
---
## Greece leads the European Space Agency’s mining of raw materials from space, which is done with net zero impact on climate and environment

{% prevButton page collections %}
![](130-greece-space.jpg)
{% nextButton page collections %}

### *Grækenland leder Det Europæiske Rum-agenturs råstofudvinding i rummet, som sker miljø- og klima-neutralt*
