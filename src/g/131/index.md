---
date: 2020-07-16
layout: post-en
permalink: "/g/131/"
---
## Norway’s renowned compentences in peacekeeping are put to use around the world

{% prevButton page collections %}
![](131-norway-peace.jpg)
{% nextButton page collections %}

### *Norges højt anerkendte kompetencer indenfor fred er til nytte mange steder i verden*
