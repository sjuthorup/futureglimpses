---
date: 2020-07-18
layout: post-en
permalink: "/g/132/"
---
## Switzerland’s long tradition for local democracy informs democratic initiatives in many different countries

{% prevButton page collections %}
![](132-swiss-democracy.jpg)
{% nextButton page collections %}

### *Schweiz’ lange tradition for lokalt demokrati inspirerer demokratiske initiativer i mange forskellige lande*
