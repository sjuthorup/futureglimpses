---
date: 2020-07-19
layout: post-en
permalink: "/g/133/"
---
## Italy’s strong tradition for cooperative business models is put to use around the world

{% prevButton page collections %}
![](133-italy-cooperatives.jpg)
{% nextButton page collections %}

### *Italiens stærke tradition for kooperative virksomhedsmodeller er til nytte verden rundt*
