---
date: 2020-07-22
layout: post-en
permalink: "/g/134/"
---
## Many countries look to Poland’s long and strong tradition of higher education in the natural sciences

{% prevButton page collections %}
![](134-poland-sciences.jpg)
{% nextButton page collections %}

### *Mange lande ser op til Polens lange og stærke tradition for højere uddannelser indenfor naturvidenskaberne*
