---
date: 2020-07-24
layout: post-en
permalink: "/g/135/"
---
## After Ireland’s restructuring of its corporate tax system in the 2020s, the country is now a paragon of fairness and sustainability

{% prevButton page collections %}
![](135-ireland-tax-scheme.jpg)
{% nextButton page collections %}

### *Efter Irlands omstrukturering af sit skattesystem i 2020’erne er landet nu et forbillede på en retfærdig og bæredygtig økonomi*
