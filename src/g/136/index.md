---
date: 2020-07-26
layout: post-en
permalink: "/g/136/"
---
## Due to its close collaboration between medical education and active medical practice, Latvia is at the forefront of patient care research

{% prevButton page collections %}
![](136-latvia-care.jpg)
{% nextButton page collections %}

### *Takket være det tætte samarbejde mellem lægevidenskab og medicinsk praksis, er Letland på forkanten af forskning i sygeplejevidenskab*
