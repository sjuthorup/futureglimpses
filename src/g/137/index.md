---
date: 2020-07-29
layout: post-en
permalink: "/g/137/"
---
## Spain has resumed its leadership in seafaring and develops modern sail ships

{% prevButton page collections %}
![](137-spain-ships.jpg)
{% nextButton page collections %}

### *Spanien har genoptaget sin lederrolle indenfor skibsfart og udvikler moderne sejlskibe*
