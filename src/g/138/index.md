---
date: 2020-07-31
layout: post-en
permalink: "/g/138/"
---
## The old lady in our building has an all-in-one walker, shopping trolley and electric vehicle

{% prevButton page collections %}
![](138-electric-walker.jpg)
{% nextButton page collections %}

### *Den gamle dame i vores opgang har en alt-i-ét rollator med elmotor og indkøbskurv*
