---
date: 2020-08-05
layout: post-en
permalink: "/g/139/"
---
## Many farmers have realized that the mandatory biodiversity strips between fields help prevent pests, diseases, and adverse weather impact on crops

{% prevButton page collections %}
![](139-biodiversity.jpg)
{% nextButton page collections %}

### *Mange bønder har opdaget, at de obligatoriske biodiversitets-baner imellem marker hjælper med at forebygge skadedyr, sygdomsangreb og skader fra vejrlig*
