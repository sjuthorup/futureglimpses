---
date: 2019-12-07
layout: post-en
permalink: "/g/14/"
---
## The Bureau of Statistics announced that services now make up 75% of the economy

{% prevButton page collections %}
![](14-services-economy.jpg)
{% nextButton page collections %}

### *Statistisk Institut opgjorde at servicesektorens andel af samfundsøkonomien nu er oppe på 75%*
