---
date: 2020-08-07
layout: post-en
permalink: "/g/140/"
---
## Mexico’s sales restrictions and high taxes on sugary products in 2020 was the start of a worldwide redefinition of what products are accepted as real food

{% prevButton page collections %}
![](140-real-food.jpg)
{% nextButton page collections %}

### *Mexicos restriktioner og beskatning på salg af sukkerholdige produkter i 2020 blev starten på en global ændring i definitionerne af, hvad der anses for ægte fødevarer*
