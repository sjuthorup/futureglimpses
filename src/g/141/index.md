---
date: 2020-08-09
layout: post-en
permalink: "/g/141/"
---
## Now that most people use the affordable night vision goggles, we avoid light pollution in the countryside, and many insect species benefit

{% prevButton page collections %}
![](141-night-goggles.jpg)
{% nextButton page collections %}

### *Nu hvor de fleste bruger de billige infrarød-briller, kan vi undgå lysforurening i landskabet, og mange insekt-arter er i fremgang*
