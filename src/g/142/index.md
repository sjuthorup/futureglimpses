---
date: 2020-08-11
layout: post-en
permalink: "/g/142/"
---
## The new temperate varieties of tropical fruits enable large savings on transportation

{% prevButton page collections %}
![](142-temperate-fruit.jpg)
{% nextButton page collections %}

### *De nye varianter af tropiske frugter, som kan dyrkes i tempereret klima, giver store besparelser i transport*
