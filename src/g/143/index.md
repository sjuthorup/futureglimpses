---
date: 2020-08-13
layout: post-en
permalink: "/g/143/"
---
## Citizens have free access to algorithms for calculating personal diets and training programs as well as ordering customized vitamin supplements

{% prevButton page collections %}
![](143-personal-diet.jpg)
{% nextButton page collections %}

### *Borgerne har gratis adgang til beregning af personlig kostplan og træningsprogram og bestilling af personligt tilpasset vitamintilskud*
