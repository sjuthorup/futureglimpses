---
date: 2020-08-16
layout: post-en
permalink: "/g/144/"
---
## Proposals for construction projects must disclose the total cost of ownership and lifecycle resource requirements, emissions and waste burden

{% prevButton page collections %}
![](144-totalcost.jpg)
{% nextButton page collections %}

### *Projektforslag for byggeri og anlæg har pligt til at afdække de samlede levetids-omkostninger, ressourcetræk, affald og udledninger*
