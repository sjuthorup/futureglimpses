---
date: 2020-08-18
layout: post-en
permalink: "/g/145/"
---
## All companies accept returns of used or obsolete products they have sold, as well as any packaging

{% prevButton page collections %}
![](145-recycle.jpg)
{% nextButton page collections %}

### *Alle virksomheder tager imod brugte og udtjente produkter de selv har solgt, samt al emballage*
