---
date: 2020-08-20
layout: post-en
permalink: "/g/146/"
---
## The think tank People Economy was awarded the Nobel Prize in Economics for their creation of tax models that prevent accumulation of capital and formation of monopolies

{% prevButton page collections %}
![](146-nobel.jpg)
{% nextButton page collections %}

### *Nobelprisen i økonomi blev tildelt tænketanken People Economy som opstillede modeller for skattesystemer, der forhindrer kapitalakkumulering og monopoldannelse*
