---
date: 2020-08-23
layout: post-en
permalink: "/g/147/"
---
## My grandad mused as he watched my nephew play: Kids nowadays don’t say vroom-vroom when driving cars…

{% prevButton page collections %}
![](147-vroom.jpg)
{% nextButton page collections %}

### *Min farfar kiggede på min legende nevø og reflekterede: Børn nu om dage brummer ikke, når de kører med biler...*
