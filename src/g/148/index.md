---
date: 2020-08-25
layout: post-en
permalink: "/g/148/"
---
## The corona crisis in 2020-21 prompted many countries to try out various models of Universal Basic Income schemes

{% prevButton page collections %}
![](148-basic-income.jpg)
{% nextButton page collections %}

### *Coronakrisen i 2020-21 fik mange lande til at afprøve forskellige former for borgerløn*
