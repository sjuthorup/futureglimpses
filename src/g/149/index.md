---
date: 2020-08-27
layout: post-en
permalink: "/g/149/"
---
## Most companies offer flexible hours and part-time, and the universal basic income allows people to adjust their work according to their needs in life

{% prevButton page collections %}
![](149-pregnancy-leave.jpg)
{% nextButton page collections %}

### *De fleste virksomheder tilbyder flextid og deltid, og borgerlønnen gør at man kan justere sin arbejdstid efter behov igennem livet*
