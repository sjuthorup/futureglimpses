---
date: 2019-12-08
layout: post-en
permalink: "/g/15/"
---
## Our literature teacher explained how 'Norwegian hypochrisy' came to be such a derogatory phrase

{% prevButton page collections %}
![](15-norwegian-hypochrisy.jpg)
{% nextButton page collections %}

### *Vores sproglærer forklarede oprindelsen til den nedsættende betegnelse "norsk dobbeltmoral"*
