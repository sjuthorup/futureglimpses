---
date: 2020-09-02
layout: post-en
permalink: "/g/151/"
---
## Transferring my profile to a new chat forum, I remember it was only in the mid-2020s that people got the ownership of their own data

{% prevButton page collections %}
![](151-own-data.jpg)
{% nextButton page collections %}

### *Mens jeg flytter min profil til et nyt chat-forum, kommer jeg i tanker om at det først var i 2020’erne, at folk fik ejerskab over deres egne data*
