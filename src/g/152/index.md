---
date: 2020-09-04
layout: post-en
permalink: "/g/152/"
---
## A coalition of rich and poor countries is working in the UN for a long-term vision of dedicating half the Earth for untouched nature

{% prevButton page collections %}
![](152-half-earth.jpg)
{% nextButton page collections %}

### *En koalition af rige og fattige lande arbejder i FN for en langsigtet vision om at dedikere halvdelen af Jorden til urørt natur*
