---
date: 2020-09-08
layout: post-en
permalink: "/g/154/"
---
## The number of extreme weather events seems to no longer be increasing, and there is hope of some leveling off in the coming years

{% prevButton page collections %}
![](154-extreme-rain.jpg)
{% nextButton page collections %}

### *Det ser ud til at tilfældene af ekstremt vejr ikke længere øges, og der er håb om et vist fald i de kommende år*
