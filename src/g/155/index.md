---
date: 2020-09-09
layout: post-en
permalink: "/g/155/"
---
## Modern wind turbines are designed to take advantage of the more frequent extreme wind speeds

{% prevButton page collections %}
![](155-extreme-wind.jpg)
{% nextButton page collections %}

### *Moderne vindmøller er designet sådan, at de kan udnytte den øgede forekomst af ekstreme vindhastigheder*
