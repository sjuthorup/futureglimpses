---
date: 2020-09-11
layout: post-en
permalink: "/g/156/"
---
## All land owners must ensure high biodiversity on at least 20% of the area

{% prevButton page collections %}
![](156-land-owner.jpg)
{% nextButton page collections %}

### *Alle der ejer et stykke jord har pligt til sikre høj biodiversitet på mindst 20% af arealet*
