---
date: 2020-09-16
layout: post-en
permalink: "/g/158/"
---
## The ‘small scale, long term’ philosophy has revolutionized disciplines from local food production to international infrastructure projects

{% prevButton page collections %}
![](158-longterm.jpg)
{% nextButton page collections %}

### *Filosofien om ‘lille skala, lang horisont’ har revolutioneret fag og brancher, fra lokal fødevareproduktion til internationale infrastrukturprojekter*
