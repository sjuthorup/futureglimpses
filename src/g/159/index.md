---
date: 2020-09-17
layout: post-en
permalink: "/g/159/"
---
## Watching an old movie with my 10 year old cousin, I found it really hard to explain the concept of “shopping”

{% prevButton page collections %}
![](159-explain-shopping.jpg)
{% nextButton page collections %}

### *Jeg så en gammel film sammen med min 10-årige kusine og fandt det meget svært at forklare begrebet “shopping”*
