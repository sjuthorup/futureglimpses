---
date: 2020-09-18
layout: post-en
permalink: "/g/160/"
---
## The system of tradeable Biodiversity and Natural Resources Certificates protects rainforest while providing substantial income for countries such as Brazil and Congo

{% prevButton page collections %}
![](160-tradeable-certificates.jpg)
{% nextButton page collections %}

### *Systemet med biodiversitetsforpligtelser, der kan handles mellem lande, beskytter regnskov og giver god indtægt til lande som Brasilien og Congo*
