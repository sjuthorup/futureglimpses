---
date: 2020-09-20
layout: post-en
permalink: "/g/161/"
---
## Most real joys don’t depend on money or technology

{% prevButton page collections %}
![](161-real-joy.jpg)
{% nextButton page collections %}

### *De fleste ægte glæder afhænger ikke af penge eller teknologi*
