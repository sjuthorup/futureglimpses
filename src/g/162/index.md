---
date: 2020-09-21
layout: post-en
permalink: "/g/162/"
---
## One of our student mates loves to cook, while the rest of us love to eat the results

{% prevButton page collections %}
![](162-cook.jpg)
{% nextButton page collections %}

### *En af vores studiekammerater elsker at lave mad, og vi andre elsker at spise resultaterne*
