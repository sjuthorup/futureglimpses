---
date: 2020-09-22
layout: post-en
permalink: "/g/163/"
---
## Competition rules ensure that tech platform companies are run separately from content production business

{% prevButton page collections %}
![](163-competition-rules.jpg)
{% nextButton page collections %}

### *Konkurrenceregler sikrer, at tech-platforms-virksomheder drives forretningsmæssigt adskilt fra indholdsproduktion*
