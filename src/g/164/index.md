---
date: 2020-09-24
layout: post-en
permalink: "/g/164/"
---
## Policies against “revolving doors” have caused many retired public servants to teach at universities, creating the added benefit of better quality and relevance of the social science studies

{% prevButton page collections %}
![](164-against-revolving.jpg)
{% nextButton page collections %}

### *Regler mod insider-lobbyisme har gjort, at mange tidligere ledere i det offentlige underviser på universiteterne, hvilket samtidig har gjort samfunds-studierne bedre og mere aktuelle*
