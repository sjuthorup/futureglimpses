---
date: 2020-09-26
layout: post-en
permalink: "/g/165/"
---
## My friend took me to see one of the recent successes in her work at the Rewilding Agency

{% prevButton page collections %}
![](165-friend-success.jpg)
{% nextButton page collections %}

### *Min ven tog mig med hen at se et af de vellykkede projekter i hendes arbejde hos Naturgenskabelsesstyrelsen*
