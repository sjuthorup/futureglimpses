---
date: 2020-09-29
layout: post-en
permalink: "/g/166/"
---
## Forecasts show that the world will never need new asphalt, due to recycling and the lower need of parking spaces

{% prevButton page collections %}
![](166-no-more-asphalt.jpg)
{% nextButton page collections %}

### *Fremskrivninger viser, at verden aldrig vil behøve ny asfalt, på grund af genbrug og det mindre behov for parkeringspladser*
