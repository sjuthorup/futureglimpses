---
date: 2020-09-30
layout: post-en
permalink: "/g/167/"
---
## Most of the world uses the UN Blockchain ID system which ensures unambiguous individual identification while protecting personal data

{% prevButton page collections %}
![](167-blockchain-id.jpg)
{% nextButton page collections %}

### *Det meste af verden bruger FNs Blockchain ID system, som sikrer entydig person-identifikation, samtidig med at persondata beskyttes*
