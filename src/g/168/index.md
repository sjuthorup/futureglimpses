---
date: 2020-10-02
layout: post-en
permalink: "/g/168/"
---
## After ten years of intense legal challenges to false and exaggerated product claims, companies have settled for a more modest advertisement style

{% prevButton page collections %}
![](168-moderate-marketing.jpg)
{% nextButton page collections %}

### *Efter ti års sagsanlæg mod falsk og overdrevet reklame, har virksomheder valgt en mere behersket tone i deres produkt-anprisninger*
