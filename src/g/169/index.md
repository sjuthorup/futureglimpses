---
date: 2020-10-04
layout: post-en
permalink: "/g/169/"
---
## Everybody working in marketing are certified in diversity and inclusive communication

{% prevButton page collections %}
![](169-inclusive-marketing.jpg)
{% nextButton page collections %}

### *Alle der arbejder indenfor markedsføring er certificeret i diversitet og inklusiv kommunikation*
