---
date: 2019-12-10
layout: post-en
permalink: "/g/17/"
---

## My mom heads the research team who invented the technology to transmit hydrogen safely in former natural-gas pipelines

{% prevButton page collections %}
![](17-mom-hydrogen-pipeline.jpg)
{% nextButton page collections %}

### *Min mor leder den forskergruppe, som opfandt teknologien til at lede brint på en sikker måde i tidligere naturgas-rørledninger*
