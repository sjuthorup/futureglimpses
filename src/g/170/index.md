---
date: 2020-10-05
layout: post-en
permalink: "/g/170/"
---
## At my insect counting trip today, I found a rhinoceros beetle of a species that was presumed extinct until it was rediscovered last year

{% prevButton page collections %}
![](170-rhino-beetle.jpg)
{% nextButton page collections %}

### *På min insekttælletur idag fandt jeg en næsehornsbille af en art, som var formodet uddød indtil den blev genopdaget sidste år*
