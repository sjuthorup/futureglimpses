---
date: 2020-10-07
layout: post-en
permalink: "/g/171/"
---
## The university held a hack day to help improve the continent-wide monitoring of insect populations

{% prevButton page collections %}
![](171-hack-day.jpg)
{% nextButton page collections %}

### *Universitetet holdt en hack-dag for at medvirke til at forbedre overvågningen af Europas insektbestande*
