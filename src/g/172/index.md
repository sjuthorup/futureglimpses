---
date: 2020-10-08
layout: post-en
permalink: "/g/172/"
---
## When Europe’s royal families took the green transition to heart, they helped create a great shift of attitude in the public

{% prevButton page collections %}
![](172-royal-transition.jpg)
{% nextButton page collections %}

### *Da Europas kongehuse tog den grønne omstilling til sig, bidrog de til et stort skift i folkestemningen*
