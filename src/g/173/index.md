---
date: 2020-10-11
layout: post-en
permalink: "/g/173/"
---
## Kindergartens and care homes are often co-located for the benefit of both young and old

{% prevButton page collections %}
![](173-colocated.jpg)
{% nextButton page collections %}

### *Børnehaver og plejehjem anlægges tit ved siden af hinanden, til glæde for både unge og gamle*
