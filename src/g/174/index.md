---
date: 2020-10-12
layout: post-en
permalink: "/g/174/"
---
## Adoption costs are fully covered by public funding

{% prevButton page collections %}
![](174-adoption.jpg)
{% nextButton page collections %}

### *Alle omkostninger til adoption dækkes af det offentlige*
