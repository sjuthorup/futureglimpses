---
date: 2020-10-13
layout: post-en
permalink: "/g/175/"
---
## As more and more forests are left unexploited, the average age of European forests is steadily growing

{% prevButton page collections %}
![](175-unexploited-forest.jpg)
{% nextButton page collections %}

### *I takt med at flere og flere skove lades uudnyttet, er gennemsnitsalderen for de europæiske skove støt stigende*
