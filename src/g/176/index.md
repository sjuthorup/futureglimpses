---
date: 2020-10-14
layout: post-en
permalink: "/g/176/"
---
## Cities are organized to suit walking and cycling traffic, and car lanes are moved underground

{% prevButton page collections %}
![](176-cycling-traffic.jpg)
{% nextButton page collections %}

### *Byer indrettes til gående og cyklende, og bilkørebaner lægges i tunneller*
