---
date: 2020-10-15
layout: post-en
permalink: "/g/177/"
---
## Both the EU and national governments are proactive in seeking out dialogues with citizens before they make a law

{% prevButton page collections %}
![](177-proactive-govt.jpg)
{% nextButton page collections %}

### *Både EU og de enkelte regeringer er proaktive og opsøger dialog med borgerne, før de laver en lov*
