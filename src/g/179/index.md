---
date: 2020-10-18
layout: post-en
permalink: "/g/179/"
---
## Prepared food comes in durable, standardized containers that carry a high deposit, ensuring recirculation

{% prevButton page collections %}
![](179-food-containers.jpg)
{% nextButton page collections %}

### *Færdigmad leveres i solide standardiserede beholdere belagt med en høj pant, der sikrer genbrug*
