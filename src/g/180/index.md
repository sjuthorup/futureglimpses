---
date: 2020-10-19
layout: post-en
permalink: "/g/180/"
---
## Boxes for postal packages come in a range of standard sizes, each with a high deposit that ensures recirculation

{% prevButton page collections %}
![](180-mail-packages.jpg)
{% nextButton page collections %}

### *Papæskerne til postpakker findes i en række standardiserede størrelser, alle med pant for at sikre genbrug*
