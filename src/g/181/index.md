---
date: 2020-10-20    
layout: post-en
permalink: "/g/181/"
---
## The European Parliament decided to double the EU’s contributions to the Climate Resilience Fund for developing countries

{% prevButton page collections %}
![](181-climate-resilience.jpg)
{% nextButton page collections %}

### *EU-parlamentet besluttede at fordoble EUs betaling til Klimatilpasningsfonden for udviklingslande*
