---
date: 2020-10-21    
layout: post-en
permalink: "/g/182/"
---
## Progress in automated translation has opened rich sources of medical knowledge from China and the former Soviet countries about treatments that aren’t based on patents

{% prevButton page collections %}
![](182-nonpatented-medical.jpg)
{% nextButton page collections %}

### *Fremskridtene i automatisk oversættelse har åbnet rige kilder af medicinsk viden fra Kina og det tidligere Sovjet, om behandlingsmetoder der ikke er patentbelagt*
