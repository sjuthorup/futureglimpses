---
date: 2020-10-22 
layout: post-en
permalink: "/g/183/"
---
## Job applications are anonymized until the first interview; now the diversity in most industries approaches that of the general population

{% prevButton page collections %}
![](183-anonymous-cv.jpg)
{% nextButton page collections %}

### *Jobansøgninger er anonymiserede indtil første samtale; nu er medarbejdersammensætningen i de fleste brancher stort set ligesom i befolkningen*
