---
date: 2020-10-25
layout: post-en
permalink: "/g/184/"
---
## Many people take new education and training to change their career in the course of life; this makes for interesting colleagues and holistic decision-making

{% prevButton page collections %}
![](184-continued-studies.jpg)
{% nextButton page collections %}

### *Mange tager en ny uddannelse og skifter karriere i løbet af livet; det giver både interessante kolleger og gode helhedsvurderinger*
