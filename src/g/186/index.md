---
date: 2020-10-28
layout: post-en
permalink: "/g/186/"
---
## A few years after the Pleistocene Park in Siberia introduced re-created mammoths, it is evident that their heavy tread and eating habits contribute to carbon binding in the ground

{% prevButton page collections %}
![](186-mammoths.jpg)
{% nextButton page collections %}

### *Få år efter at Palæoparken i Sibirien udsatte genskabte mammutter, er det tydeligt at deres tunge skridt og spisevaner bidrager til at binde kulstof i jorden*
