---
date: 2020-10-29
layout: post-en
permalink: "/g/187/"
---
## Former oil engineers deploy their competences to establish geothermal energy plants for the disctrict heating sector

{% prevButton page collections %}
![](187-geothermal-engineers.jpg)
{% nextButton page collections %}

### *Tidligere olie-ingeniører bruger deres kompetencer til etablering af geotermi til fjernvarmesektoren*
