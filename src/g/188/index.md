---
date: 2020-10-30
layout: post-en
permalink: "/g/188/"
---
## Many solar electricity farms keep sheep or chicken among the panels in the fields

{% prevButton page collections %}
![](188-pv-sheep.jpg)
{% nextButton page collections %}

### *Mange solstrømsanlæg holder får og høns blandt solpanelerne*
