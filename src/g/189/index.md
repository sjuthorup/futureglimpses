---
date: 2020-11-01
layout: post-en
permalink: "/g/189/"
---
## A new industry has formed around the cleansing, disassembly and recycling of old oil platforms and refinery equipment

{% prevButton page collections %}
![](189-steel.jpg)
{% nextButton page collections %}

### *Der er opstået en ny industri indenfor rensning, ophugning og genbrug af gamle olieplatforme og raffinaderi-udstyr*
