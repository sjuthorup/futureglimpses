---
date: 2019-12-12
layout: post-en
permalink: "/g/19/"
---
## It’s my birthday, and we’re on a fabulous drone safari among humpback whales west of Greenland, watching them up close and hearing them sing

{% prevButton page collections %}
![](19-drone-safari-graytones.jpg)
{% nextButton page collections %}

### *Det er min fødselsdag, og vi er på en fantastisk dronesafari blandt pukkelhvaler vest for Grønland -- vi ser dem helt tæt på og hører dem synge*
