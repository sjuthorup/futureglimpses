---
date: 2020-11-02
layout: post-en
permalink: "/g/190/"
---
## I volunteered at one of the city council simulation events that are conducted with the older school classes

{% prevButton page collections %}
![](190-council-simulation.jpg)
{% nextButton page collections %}

### *Jeg var med som frivillig hjælper på en af byråds-simuleringerne med de store skoleklasser*
