---
date: 2020-11-05
layout: post-en
permalink: "/g/192/"
---
## The rule that banks must lend only to the real economy is now fully in force; home prices have fallen to half the level of 2020

{% prevButton page collections %}
![](192-home-prices.jpg)
{% nextButton page collections %}

### *Nu er reglen om, at banker kun må udlåne til realøkonomien, fuldt indfaset; boligpriserne er faldet til det halve af niveauet i 2020*
