---
date: 2020-11-07
layout: post-en
permalink: "/g/193/"
---
## School classes pick a local species as their mascot and compete in which populations thrive the best

{% prevButton page collections %}
![](193-school-mascot.jpg)
{% nextButton page collections %}

### *Skoleklasser vælger en dyreart som maskot og konkurrerer om, hvilke bestande der trives bedst*
