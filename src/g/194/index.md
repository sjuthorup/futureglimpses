---
date: 2020-11-09
layout: post-en
permalink: "/g/194/"
---
## Many companies pick an animal species or a specific local ecology which they support and report on

{% prevButton page collections %}
![](194-report-animal.jpg)
{% nextButton page collections %}

### *Mange virksomheder vælger en dyreart eller en bestemt lokal økologi, som de støtter og rapporterer om*
