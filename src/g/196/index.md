---
date: 2020-11-11
layout: post-en
permalink: "/g/196/"
---
## All research funded by public means must give free access to the underlying data

{% prevButton page collections %}
![](196-research-funding.jpg)
{% nextButton page collections %}

### *Al forskning der finansieres af offentlige midler har pligt til at give fri adgang til deres underliggende data*
