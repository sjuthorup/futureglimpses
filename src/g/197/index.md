---
date: 2020-11-13
layout: post-en
permalink: "/g/197/"
---
## Some new varieties of beans and lentils contain the amino acids otherwise mainly found in animal products

{% prevButton page collections %}
![](197-amino-beans.jpg)
{% nextButton page collections %}

### *Visse nye sorter af bønner og linser indeholder de aminosyrer, som ellers kun fås fra kødprodukter*
