---
date: 2019-10-13
layout: post-en
permalink: "/g/2/"
---

## At my grandma's funeral, one of her former colleagues recounted how she was the first employee at their company to start a climate strike

{% prevButton page collections %}
![](2-grandmother-strike.jpg)
{% nextButton page collections %}

### *Ved min bedstemors begravelse fortalte en af hendes tidligere kolleger, hvordan hun som den første på deres arbejdsplads organiserede en klimastrejke*
