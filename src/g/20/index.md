---
date: 2019-12-13
layout: post-en
permalink: "/g/20/"
---
## My brother got an exciting job as a maintenance engineer at the Dogger Bank energy island

{% prevButton page collections %}
![](20-dogger-banke.jpg)
{% nextButton page collections %}

### *Min bror fik et spændende job som vedligeholdelsesingeniør på Doggerbanke-energiøen*
