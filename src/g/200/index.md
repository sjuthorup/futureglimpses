---
date: 2020-11-17
layout: post-en
permalink: "/g/200/"
---
## The simple medical procedure for reversible sterilization has made a huge difference for women’s options and positions in society, all over the world

{% prevButton page collections %}
![](200-womens-options.jpg)
{% nextButton page collections %}

### *Den enkle sterilisations-operation, som kan omgøres, har haft en enorm betydning for kvinders muligheder og samfundspositioner overalt i verden*
