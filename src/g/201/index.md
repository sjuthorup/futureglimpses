---
date: 2020-11-19
layout: post-en
permalink: "/g/201/"
---
## Sometimes we don’t believe my grandad when he talks about his youth; today he said people would burn blocks of fat indoors and even burn poisonous sticks in their mouths

{% prevButton page collections %}
![](201-grandad-burn.jpg)
{% nextButton page collections %}

### *Nogle gange tror vi ikke på min farfars historier om sin ungdom; idag fortalte han at folk brændte blokke af fedtstof indendørs og endda brændte giftige pinde i munden*
