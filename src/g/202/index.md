---
date: 2020-11-20
layout: post-en
permalink: "/g/202/"
---
## Collaborative artwork and performances often use 3D tools so artists from anywhere in the world can participate without traveling

{% prevButton page collections %}
![](202-collab-art.jpg)
{% nextButton page collections %}

### *Samarbejder om kunstværker og optræden sker via 3D-værktøjer, så kunstnere fra hele verden kan deltage uden at rejse*
