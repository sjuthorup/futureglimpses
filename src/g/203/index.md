---
date: 2020-11-22
layout: post-en
permalink: "/g/203/"
---
## We are joining in on the hilarious game “move like a local beetle” that was invented by a Fijian last week and put into 3D simulation by a Congolese over the weekend

{% prevButton page collections %}
![](203-beetle-game.jpg)
{% nextButton page collections %}

### *Vi spiller med i det sjove spil “bevæg dig som en lokal bille”, som blev opfundet af en fijianer i sidste uge og lavet til en 3D-simulering af en congoleser i løbet af weekenden*
