---
date: 2020-11-24
layout: post-en
permalink: "/g/204/"
---
## Almost all plastic is sourced from ocean cleanup

{% prevButton page collections %}
![](204-ocean-plastic.jpg)
{% nextButton page collections %}

### *Næsten al plastik er fremstillet fra opsamling i havet*
