---
date: 2020-11-26
layout: post-en
permalink: "/g/205/"
---
## Government communicates law changes in a timely manner, and most people accept that schemes benefitting them may be changed to further the green transition

{% prevButton page collections %}
![](205-tax-message.jpg)
{% nextButton page collections %}

### *Regeringen informerer om lovændringer i god tid, og de fleste borgere accepterer at ordninger, de har haft glæde af, kan ændres af hensyn til den grønne omstilling*
