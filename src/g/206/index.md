---
date: 2020-11-27
layout: post-en
permalink: "/g/206/"
---
## EU countries compete in having the largest contiguous nature preserve. This year’s winner is the expanded Triglav nature park, shared by Slovenia, Austria and Italy

{% prevButton page collections %}
![](206-ibex.jpg)
{% nextButton page collections %}

### *EU-landene konkurrerer om at have det største sammenhængende naturområde. I år er vinderen den udvidede Triglav-naturpark, som Slovenien, Østrig og Italien er fælles om*
