---
date: 2020-11-30
layout: post-en
permalink: "/g/207/"
---
## Schools teach democracy, ethics and humanism, not religion

{% prevButton page collections %}
![](207-no-religion.jpg)
{% nextButton page collections %}

### *Skolerne underviser i demokrati, etik og humanisme, ikke i religion*
