---
date: 2020-12-01
layout: post-en
permalink: "/g/208/"
---
## Some good can come from tragedies. Like the pandemic in 1918 helped end the world war, the pandemic in 2020 helped end industrial farming and fossil-fueled flying

{% prevButton page collections %}
![](208-some-good.jpg)
{% nextButton page collections %}

### *Noget godt kan opstå af tragedier. Ligesom pandemien i 1918 hjalp med at stoppe verdenskrigen, hjalp pandemien i 2020 med at afvikle industrilandbrug og fossildrevet flyvning*
