---
date: 2020-12-02
layout: post-en
permalink: "/g/209/"
---
## Today we learned about the profound effects of news medias’ decision to use the words “society” and “citizens” rather than the once-ubiquitous “the economy” and “consumers”

{% prevButton page collections %}
![](209-not-consumers.jpg)
{% nextButton page collections %}

### *Idag lærte vi om den enorme effekt af nyhedsmediers beslutning om at bruge ord som “samfundet” og “borgere” fremfor de engang udbredte “økonomien” og “forbrugere”*
