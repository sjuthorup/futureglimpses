---
date: 2020-12-03
layout: post-en
permalink: "/g/210/"
---
## The last global investment bank is going bankrupt after the EU Supreme Court’s conviction of reckless and short-term-biased business conduct

{% prevButton page collections %}
![](210-bank-conviction.jpg)
{% nextButton page collections %}

### *Den sidste globale investeringsbank går konkurs efter EU Højesterets dom om hensynsløs og kortsigtet forretningsførelse*
