---
date: 2020-12-04
layout: post-en
permalink: "/g/211/"
---
## I had an interesting chat with my grandad about the ways many jobs have been transformed in the last few decades

{% prevButton page collections %}
![](211-grandad-jobs.jpg)
{% nextButton page collections %}

### *Jeg havde en spændende snak med min farfar om, hvordan mange jobs er forandret i løbet af de seneste årtier*
