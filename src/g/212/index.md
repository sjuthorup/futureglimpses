---
date: 2020-12-06
layout: post-en
permalink: "/g/212/"
---
## Transformed jobs: Metal smiths are as skilled at recycling  materials from old products as they are at creating new products

{% prevButton page collections %}
![](212-metal-smith.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Smede ved lige så meget om at genbruge materialer fra gamle produkter som om at lave nye produkter*
