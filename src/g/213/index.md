---
date: 2020-12-08
layout: post-en
permalink: "/g/213/"
---
## Transformed jobs: Former installers of oil and gas boilers mount solar energy and heatpumps

{% prevButton page collections %}
![](213-installers.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Tidligere installatører af olie- og gas-fyr etablerer solenergi og varmepumper*
