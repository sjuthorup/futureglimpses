---
date: 2020-12-10
layout: post-en
permalink: "/g/214/"
---
## Transformed jobs: Builders work mainly on repairing and maintaining existing contructions, and most materials come from recycling

{% prevButton page collections %}
![](214-builders.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Håndværkere laver hovedsagelig reparationer og vedligeholdelse, og de fleste materialer er genbrug*
