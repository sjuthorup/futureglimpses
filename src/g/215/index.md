---
date: 2020-12-11
layout: post-en
permalink: "/g/215/"
---
## Transformed jobs: Fishermen help preserve and support the growth of fish stocks, and they only catch species that are growing in numbers

{% prevButton page collections %}
![](215-fishermen.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Fiskere hjælper med til at bevare og understøtte væksten af fiskebestande, og de fanger kun arter der er i fremgang*
