---
date: 2020-12-13
layout: post-en
permalink: "/g/216/"
---
## Transformed jobs: Chefs mainly cook plant-based dishes, and only a few are specialized in cooking with meat

{% prevButton page collections %}
![](216-chefs.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Kokke laver hovedsagelig plantebaseret mad, og kun få er specialiseret i at lave kød-retter*
