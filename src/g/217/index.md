---
date: 2020-12-14
layout: post-en
permalink: "/g/217/"
---
## Transformed jobs: Teachers mainly give individual help, and great topic presentations are online

{% prevButton page collections %}
![](217-individual-help.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Lærere giver især individuel hjælp, og gode emne-præsentationer ses online*
