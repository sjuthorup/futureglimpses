---
date: 2020-12-15
layout: post-en
permalink: "/g/218/"
---
## Transformed jobs: Drivers maintain the self-driving cars, assess delivery routes, have a dialogue with customers and people along the route about traffic optimizations

{% prevButton page collections %}
![](218-drivers.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Chauffører vedligeholder de selvkørende biler, vurderer ruter, taler med kunder og folk langs ruten om optimeringer af færdslen*
