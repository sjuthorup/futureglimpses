---
date: 2020-12-16
layout: post-en
permalink: "/g/219/"
---
## Transformed jobs: Lawmakers base rules and budgets on robust data about historic results

{% prevButton page collections %}
![](219-lawmakers-facts.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Lovgivere baserer regler og budgetter på et solidt datagrundlag om historiske resultater*
