---
date: 2019-12-15
layout: post-en
permalink: "/g/22/"
---
## The Carbon Sequestration plant is the pride of our town, providing 25% of the local jobs

{% prevButton page collections %}
![](22-carbon-sequestration.jpg)
{% nextButton page collections %}

### *Vores by er stolt af CO2-lagringsanlægget, som står for 25% af de lokale arbejdspladser*
