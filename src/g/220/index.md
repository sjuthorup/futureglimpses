---
date: 2020-12-18
layout: post-en
permalink: "/g/220/"
---
## Transformed jobs: Doctors make decisions with full access to the most recent research

{% prevButton page collections %}
![](220-doctors.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Læger træffer beslutninger med fuld adgang til nyeste forskningsresultater*
