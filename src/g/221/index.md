---
date: 2020-12-20
layout: post-en
permalink: "/g/221/"
---
## Transformed jobs: Shop assistants can focus on fulfilling customer needs because sales and logistics are fully automated

{% prevButton page collections %}
![](221-shop-assistant.jpg)
{% nextButton page collections %}

### *Forandrede jobs: Butikspersonale kan fokusere på at opfylde kundebehovene, fordi salg og levering er fuldt automatiseret*
