---
date: 2020-12-23
layout: post-en
permalink: "/g/223/"
---
## Presents for holidays are seldom purchased items, but rather poems, songs, paintings, and gift cards for experiences
{% prevButton page collections %}
![](223-presents.jpg)
{% nextButton page collections %}
### *Gaver til højtider er sjældent købte ting, men som regel digte, sange, malerier, og gavekort til oplevelser*
