---
date: 2020-12-25
layout: post-en
permalink: "/g/224/"
---
## Parents are conscious that nature is crucial for children’s development and make sure their kids play in a natural environment several times a week
{% prevButton page collections %}
![](224-play-nature.jpg)
{% nextButton page collections %}
### *Forældre er bevidste om, at natur er afgørende for børns udvikling, og sørger for at deres børn leger ude i naturen flere gange om ugen*
