---
date: 2020-12-26
layout: post-en
permalink: "/g/225/"
---
## Our whole family visited the Aquarium on the occasion of Big Healthy Oceans Week, a celebration that 30% of European waters are now fully protected from human activity
{% prevButton page collections %}
![](225-aquarium.jpg)
{% nextButton page collections %}
### *Hele familien var i akvariet i anledning af Stort Sundt Hav-ugen, som fejrer at hele 30% af de europæiske farvande nu er fuldt beskyttet mod menneskelig aktivitet*
