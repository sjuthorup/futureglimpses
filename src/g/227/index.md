---
date: 2020-12-30
layout: post-en
permalink: "/g/227/"
---
## Self-directed mini-drones enable authorities and citizens to detect pollution and other corporate crime
{% prevButton page collections %}
![](227-mini-drone.jpg)
{% nextButton page collections %}
### *Selvstyrende minidroner hjælper myndigheder og borgere med at afsløre forurening og anden erhvervskriminalitet*
