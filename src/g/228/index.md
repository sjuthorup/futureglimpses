---
date: 2021-01-03
layout: post-en
permalink: "/g/228/"
---
## Fishing nets are registered and monitored, and any loss of nets at sea incurs a large fine; ocean plastic pollution is just 1 percent of the 2020s level
{% prevButton page collections %}
![](228-fishing-nets.jpg)
{% nextButton page collections %}
### *Fiskenet bliver registreret og overvåget, og tab af net i havet straffes med en høj bøde; plastikforureningen i havene er nu kun 1 procent af, hvad den var i 2020’erne*
