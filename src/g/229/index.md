---
date: 2021-01-05
layout: post-en
permalink: "/g/229/"
---
## Automated translation helps teachers give minority children fully updated knowledge in their own language
{% prevButton page collections %}
![](229-minority-languages.jpg)
{% nextButton page collections %}
### *Automatisk oversættelse hjælper lærere med at give minoritetsbørn fuldt opdateret viden på deres eget sprog*
