---
date: 2021-01-12
layout: post-en
permalink: "/g/231/"
---
## Some countries allow young people from the age of 14 to vote, provided they take a training course in democracy
{% prevButton page collections %}
![](231-young-voter.jpg)
{% nextButton page collections %}
### *Nogle lande tillader unge mennesker fra 14 år at stemme, hvis de tager et kursus i demokrati*
