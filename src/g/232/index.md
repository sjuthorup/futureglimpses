---
date: 2021-01-13
layout: post-en
permalink: "/g/232/"
---
## Some countries are experimenting with adjusting the weight of votes according to age, to ensure that the people who will live the longest with a decision have the largest say
{% prevButton page collections %}
![](232-differentiate-votes.jpg)
{% nextButton page collections %}
### *Nogle lande eksperimenterer med at justere vægten af stemmer efter alder, sådan at dem, der skal leve længst med en beslutning, også har mest at sige*
