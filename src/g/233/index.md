---
date: 2021-01-15
layout: post-en
permalink: "/g/233/"
---
## Diamonds, gold and hardwood are registered in global blockchain systems in order to track their origin and control for labor conditions, climate footprint and pollution
{% prevButton page collections %}
![](233-diamonds.jpg)
{% nextButton page collections %}
### *Diamanter, guld og ædeltræ bliver registreret i verdensomspændende blockchain-systemer, så oprindelsen spores og kontrolleres for arbejdsvilkår, klimaaftryk og forurening*
