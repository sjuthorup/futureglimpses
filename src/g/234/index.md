---
date: 2021-01-18
layout: post-en
permalink: "/g/234/"
---
## Ravens are popular pets. They are as intelligent as dogs, and their food has a much smaller carbon footprint
{% prevButton page collections %}
![](234-raven.jpg)
{% nextButton page collections %}
### *Ravne er populære kæledyr -- lige så intelligente som hunde, og deres foder har meget mindre klimabelastning*
