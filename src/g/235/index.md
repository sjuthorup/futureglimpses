---
date: 2021-01-21
layout: post-en
permalink: "/g/235/"
---
## Resources are not spent on cremation; deceased bodies are buried in compostable coffins
{% prevButton page collections %}
![](235-burial.jpg)
{% nextButton page collections %}
### *Der bruges ikke ressourcer på kremering; afdøde bliver begravet i komposterbare kister*
