---
date: 2021-01-24
layout: post-en
permalink: "/g/236/"
---
## King Charles announced that 159 countries are on track to fulfill the Terra Carta goal of protecting 50% of lands and oceans by 2050
{% prevButton page collections %}
![](236-king-charles.jpg)
{% nextButton page collections %}
### *Kong Charles bekendtgjorde, at 159 lande forventes at opfylde Terra Carta-målet om at beskytte 50% af alt land og hav i år 2050*
