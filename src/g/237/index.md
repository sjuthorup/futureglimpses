---
date: 2021-01-27
layout: post-en
permalink: "/g/237/"
---
## The Erasmus system for study in other EU countries also encompasses nurses, mechanics, kindergarten teachers, craftsmen, and many other trades
{% prevButton page collections %}
![](237-erasmus.jpg)
{% nextButton page collections %}
### *Erasmus-udvekslingsordningen til studier i andre EU-lande omfatter også sygeplejersker, mekanikere, børnehavepædagoger, håndværkere, og mange andre fag*
