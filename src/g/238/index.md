---
date: 2021-01-28
layout: post-en
permalink: "/g/238/"
---
## Job positions involving hard physical work must include a measure of non-physical tasks in order to prevent strain and injury
{% prevButton page collections %}
![](238-non-physical.jpg)
{% nextButton page collections %}
### *Stillinger med hårdt fysisk arbejde skal også indeholde en vis andel ikke-fysiske opgaver for at hindre overbelastning og nedslidning*
