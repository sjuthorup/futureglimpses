---
date: 2021-01-29
layout: post-en
permalink: "/g/239/"
---
## Solar paint, local windturbines, and low-cost salt batteries avert large investments in power transmission cables
{% prevButton page collections %}
![](239-local-power.jpg)
{% nextButton page collections %}
### *Med solpanel-maling, lokale vindmøller og billige saltbatterier undgås store investeringer i dyre el-transmissionskabler*
