---
date: 2021-02-01
layout: post-en
permalink: "/g/240/"
---
## Traditional mining is being replaced by agro-mining: A gentle method of obtaining minerals using plants that extract specific elements from the soil
{% prevButton page collections %}
![](240-agromining.jpg)
{% nextButton page collections %}
### *Traditionel minedrift er ved at blive erstattet af agro-mining: En skånsom råstofudvinding ved hjælp af planter, der trækker bestemte grundstoffer ud af jorden*
