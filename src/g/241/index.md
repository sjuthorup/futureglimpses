---
date: 2021-02-03
layout: post-en
permalink: "/g/241/"
---
## The EU funds an extensive translation of news items to ensure a diverse press and democratic discourse across Europe
{% prevButton page collections %}
![](241-translate-news.jpg)
{% nextButton page collections %}
### *EU finansierer en omfattende oversættelse af nyhedsstof for at sikre en mangfoldig presse og demokratisk samtale på tværs af Europa*
