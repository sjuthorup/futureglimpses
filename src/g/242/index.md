---
date: 2021-02-05
layout: post-en
permalink: "/g/242/"
---
## The old concept ‘business model’ is mainly replaced by ‘life cycle position’
{% prevButton page collections %}
![](242-lifecycle.jpg)
{% nextButton page collections %}
### *Det gamle begreb “forretningsmodel” er stort set erstattet af “livscyklustrin”*
