---
date: 2021-02-10
layout: post-en
permalink: "/g/243/"
---
## Our teacher is trying to explain how the concepts ‘shareholder value’ and ‘economy of scale’ used to be perceived as positive
{% prevButton page collections %}
![](243-teacher-trying.jpg)
{% nextButton page collections %}
### *Vores lærer prøver at forklare, hvordan begreberne ‘aktionærgevinst’ og ‘storskalafordele’ engang blev opfattet positivt*
