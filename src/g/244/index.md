---
date: 2021-04-04
layout: post-en
permalink: "/g/244/"
---
## My aunt explains how our bread is more nutritious than in the industrial age because we grind the grain right before baking
{% prevButton page collections %}
![](244-aunt-bread.jpg)
{% nextButton page collections %}
### *Min tante forklarer, hvordan vores brød er mere næringsrigt end i industritiden, fordi vi maler kornet lige inden bagning*
