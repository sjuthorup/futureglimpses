---
date: 2021-04-07
layout: post-en
permalink: "/g/245/"
---
## Many small islands see an influx of residents because drone delivery is easy and affordable
{% prevButton page collections %}
![](245-island-drone.jpg)
{% nextButton page collections %}
### *Mange mindre øer oplever ny tilflytning, fordi drone-levering er nemt og billigt*
