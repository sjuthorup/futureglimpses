---
date: 2021-04-12
layout: post-en
permalink: "/g/247/"
---
## One of my electives this semester is Feedback Loops, a conceptual framework that relates to a lot of different disciplines
{% prevButton page collections %}
![](247-feedback-loops.jpg)
{% nextButton page collections %}
### *Et af mine valgfag i dette semester er feedback-loops, en begrebsramme som er relevant i mange forskellige fag*
