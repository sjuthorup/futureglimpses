---
date: 2021-04-16
layout: post-en
permalink: "/g/249/"
---
## News media are integrated with Wikipedia so you can look up explanations right away while reading or watching
{% prevButton page collections %}
![](249-wikimedia.jpg)
{% nextButton page collections %}
### *Nyhedsmedier er integreret med Wikipedia, så man umiddelbart kan slå ting op mens man læser eller kigger*
