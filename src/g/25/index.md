---
date: 2019-12-20
layout: post-en
permalink: "/g/25/"
---
## Following the Rewilding Guidelines, our town is in full swing removing pavement from public spaces

{% prevButton page collections %}
![](25-town-rewilding.jpg)
{% nextButton page collections %}

### *Vores by følger naturgenskabelses-retningslinjerne og er nu i fuld gang med at fjerne fliser og belægning fra offentlige pladser*
