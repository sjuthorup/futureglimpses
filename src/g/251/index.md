---
date: 2021-04-22
layout: post-en
permalink: "/g/251/"
---
## Our thesis project explores the democracy-enhancing elements in each of our countries
{% prevButton page collections %}
![](251-thesis-democracy.jpg)
{% nextButton page collections %}
### *Vores specialearbejde udforsker de demokrati-styrkende elementer i vores tre lande*
