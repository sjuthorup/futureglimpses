---
date: 2021-04-25
layout: post-en
permalink: "/g/252/"
---
## Danish citizens don’t need to enroll for voting; everyone automatically receives a ballot
{% prevButton page collections %}
![](252-ballot.jpg)
{% nextButton page collections %}
### *I Danmark behøver man ikke tilmelde sig som vælger; alle borgere får automatisk et valgkort*
