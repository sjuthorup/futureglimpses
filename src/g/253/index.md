---
date: 2021-04-27
layout: post-en
permalink: "/g/253/"
---
## The reunified Korea learned from Germany’s experience in the 1990s: Regional economic inequalities are purposely redressed, and an abundance of cross-national cultural exchange programs have been created
{% prevButton page collections %}
![](253-korean-festival.jpg)
{% nextButton page collections %}
### *Genforeningen af Korea lærte af Tysklands erfaringer fra 1990’erne: Regionale økonomiske uligheder blev målrettet udjævnet, og der blev skabt en mangfoldighed af tværnational kulturudveksling*
