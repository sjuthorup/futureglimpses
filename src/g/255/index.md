---
date: 2021-05-03
layout: post-en
permalink: "/g/255/"
---
## Sunset clauses are often used, ensuring that obsolete legislation is purged
{% prevButton page collections %}
![](255-sunset-clause.jpg)
{% nextButton page collections %}
### *Solnedgangsklausuler bruges ofte, så der blir luget ud i forældet lovgivning*
