---
date: 2021-05-05
layout: post-en
permalink: "/g/256/"
---
## More than twenty countries, including ours, have introduced mechanisms to ensure a minimum of 3 political parties in parliament to necessitate collaboration
{% prevButton page collections %}
![](256-political-parties.jpg)
{% nextButton page collections %}
### *Over tyve lande, inklusive vores, har indført mekanismer til at sikre, at der er mindst 3 politiske partier repræsenteret i parlamentet for at nødvendiggøre samarbejde*
