---
date: 2021-05-07
layout: post-en
permalink: "/g/257/"
---
## Korea’s open information law sets an international standard for government transparency
{% prevButton page collections %}
![](257-transparency.jpg)
{% nextButton page collections %}
### *Koreas lov om offentlighed i forvaltningen er et internationalt forbillede om åbenhed*
