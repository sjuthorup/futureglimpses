---
date: 2021-05-10
layout: post-en
permalink: "/g/258/"
---
## Uruguay’s public co-funding of media ensures that everyone, regardless of financial means, have access to reliable news and debate
{% prevButton page collections %}
![](258-media-funding.jpg)
{% nextButton page collections %}
### *Uruguays mediestøtte-ordning sikrer at alle, uanset indkomst, har adgang til pålidelig nyhedsinformation og debat*
