---
date: 2019-12-30
layout: post-en
permalink: "/g/26/"
---
## We had a long discussion about the planned gene modification of polar bears: Should humans modify them in order to save them as a species -- and can we even call them the same species?

{% prevButton page collections %}
![](26-polar-bear-GMO.jpg)
{% nextButton page collections %}
  
### *Vi havde en lang diskussion om den planlagte genmodificering af isbjørne: Bør man genmodificere dem for at redde arten -- og kan vi overhovedet kalde dem samme art?*
