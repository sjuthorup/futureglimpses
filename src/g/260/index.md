---
date: 2021-05-16
layout: post-en
permalink: "/g/260/"
---
## Denmark holds an annual Youth Parliament session that decides on an actual, youth-related law
{% prevButton page collections %}
![](260-youth-parliament.jpg)
{% nextButton page collections %}
### *Danmark holder et årligt ungdomsparlament, som vedtager en faktisk lov indenfor ungdoms-området*
