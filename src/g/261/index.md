---
date: 2021-05-19
layout: post-en
permalink: "/g/261/"
---
## Uruguay, Korea and Denmark all have free 12-year education and are among the global top 25 countries for democratic participation
{% prevButton page collections %}
![](261-education.jpg)
{% nextButton page collections %}
### *Både Uruguay, Korea og Danmark har gratis 12 års skolegang og er i den internationale top 25 for befolkningens aktive deltagelse i demokratiet*
