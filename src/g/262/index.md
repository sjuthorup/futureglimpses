---
date: 2021-05-21
layout: post-en
permalink: "/g/262/"
---
## More than 90% of the population in Uruguay, Korea and Denmark feel they can contribute to the democratic debate, and voting participation is typically above 80%
{% prevButton page collections %}
![](262-voting.jpg)
{% nextButton page collections %}
### *I Uruguay, Korea og Denmark mener mindst 90% af befolkningen, at de har mulighed for at påvirke samfundsdebatten, og valgdeltagelsen ligger normalt over 80%*
