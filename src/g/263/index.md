---
date: 2021-05-25
layout: post-en
permalink: "/g/263/"
---
## Our thesis conclusion is that true deliberative democracy is both desirable and possible
{% prevButton page collections %}
![](263-thesis-finished.jpg)
{% nextButton page collections %}
### *Vores specialekonklusion er, at ægte deliberativt demokrati er både ønskeligt og muligt*
