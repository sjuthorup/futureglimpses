---
date: 2021-06-02
layout: post-en
permalink: "/g/266/"
---
## Since the pesticide moratorium took effect, populations of all flying insects are rising, including the rarest ones
{% prevButton page collections %}
![](266-flying-insects.jpg)
{% nextButton page collections %}
### *Efter sprøjtegift-moratoriet trådte i kraft, ses stigninger i bestandene af alle flyvende insekter, også de meget sjældne*
