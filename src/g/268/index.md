---
date: 2021-06-09
layout: post-en
permalink: "/g/268/"
---
## Tests of new medication are most often done with female participants because men’s hormone cycle is too simple to reveal side effects
{% prevButton page collections %}
![](268-tests.jpg)
{% nextButton page collections %}
### *Til ny medicin bruger man normalt kvindelige testpersoner, fordi mænds hormoncyklus er for enkel til at afsløre bivirkninger*
