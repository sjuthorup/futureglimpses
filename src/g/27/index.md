---
date: 2019-12-30
layout: post-en
permalink: "/g/27/"
---
## New Year’s always makes me think of our future as global humanity and all the possibilities that lie ahead

{% prevButton page collections %}
![](27-new-years.jpg)
{% nextButton page collections %}

### *Nytår får mig altid til at tænke på vores fremtid som global menneskehed, og alle de muligheder vi har foran os*
