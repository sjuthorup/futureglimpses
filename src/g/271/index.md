---
date: 2021-06-15
layout: post-en
permalink: "/g/271/"
---
## Passports no longer require gender information
{% prevButton page collections %}
![](271-passports.jpg)
{% nextButton page collections %}
### *Pas kræver ikke længere specifikation af køn*
