---
date: 2021-06-16
layout: post-en
permalink: "/g/272/"
---
## Since the 2020s property bubble, the EU has ensured social mobility via high taxes on inheritance and property gains
{% prevButton page collections %}
![](272-mobility.jpg)
{% nextButton page collections %}
### *Siden 2020’ernes ejendomsboble har EU sikret den sociale mobilitet ved hjælp af høje skatter på arv og ejendomsgevinster*
