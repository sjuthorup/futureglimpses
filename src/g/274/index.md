---
date: 2021-06-21
layout: post-en
permalink: "/g/274/"
---
## Due to the increasing number of families with one or no children, many people have begun sharing a home with friends
{% prevButton page collections %}
![](274-friends.jpg)
{% nextButton page collections %}
### *Da der efterhånden er mange familier med enebørn eller ingen børn, er mange begyndt at bo sammen med venner*
