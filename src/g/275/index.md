---
date: 2021-06-23
layout: post-en
permalink: "/g/275/"
---
## I am moving to a building with positive climate balance, rain water collection and a salamander pond in the back garden
{% prevButton page collections %}
![](275-apartment.jpg)
{% nextButton page collections %}
### *Jeg flytter til en ejendom med plus på klimaregnskabet, regnvandsopsamling og salamander-vandhul i baghaven*
