---
date: 2021-06-25
layout: post-en
permalink: "/g/276/"
---
## City planning aims for everybody to be able to walk within 5 minutes to grocery shopping, public transport, park and playground
{% prevButton page collections %}
![](276-city.jpg)
{% nextButton page collections %}
### *Byplanlægning har som mål, at alle skal kunne gå på højst 5 minutter til indkøb, offentlig transport, park og legeplads*
