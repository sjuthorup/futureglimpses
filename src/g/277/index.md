---
date: 2021-06-28
layout: post-en
permalink: "/g/277/"
---
## My friend at the Rewilding Agency talks about the benefits from wildlife corridors being made to favor animals, not traffic
{% prevButton page collections %}
![](277-corridor.jpg)
{% nextButton page collections %}
### *Min ven i Naturgenskabelsesstyrelsen fortæller, at det hjælper dyrelivet meget, når vildt-korridorer gør passage nemmest for dyrene, ikke trafikken*
