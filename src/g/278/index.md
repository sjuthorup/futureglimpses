---
date: 2021-06-30
layout: post-en
permalink: "/g/278/"
---
## A range of crowdfunding models are replacing the old-fashioned revenue models based on copyright and patents
{% prevButton page collections %}
![](278-crowdfunding.jpg)
{% nextButton page collections %}
### *Forskellige crowdfunding-modeller er ved at erstatte de gammeldags copyright- og patent-baserede indtjeningsmodeller*
