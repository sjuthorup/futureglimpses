---
date: 2021-07-02
layout: post-en
permalink: "/g/279/"
---
## With my degree in sustainable technology management I have a wide range of job options
{% prevButton page collections %}
![](279-degree.jpg)
{% nextButton page collections %}
### *Med min kandidatgrad i bæredygtig teknologiledelse har jeg mange jobmuligheder*
