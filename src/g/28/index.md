---
date: 2019-12-30
layout: post-en
permalink: "/g/28/"
---
## As always on New Year’s Eve, my granddad told us he loves the modern kind of fireworks made from water spray and light

{% prevButton page collections %}
![](28-fireworks.jpg)
{% nextButton page collections %}

### *Som altid på nytårsaften fortalte min farfar, hvor glad han er for det moderne fyrværkeri baseret på vanddråber og lys*
