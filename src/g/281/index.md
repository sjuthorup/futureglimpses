---
date: 2021-07-07
layout: post-en
permalink: "/g/281/"
---
## Yay! I got a job as a controller with HumusLabs, a company that certifies carbon sequestration in soil
{% prevButton page collections %}
![](281-humuslabs.jpg)
{% nextButton page collections %}
### *Jubi! Jeg har fået en stilling som kontrolmedarbejder hos HumusLabs, et firma der certificerer kulstoflagring i muld*
