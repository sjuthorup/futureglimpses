---
date: 2021-07-09
layout: post-en
permalink: "/g/282/"
---
## The artificial intelligence in our remote sensing equipment can explain how it reaches its results so we can adjust the logic if necessary
{% prevButton page collections %}
![](282-explain.jpg)
{% nextButton page collections %}
### *Den kunstige intelligens i vores fjernmålingsudstyr kan forklare, hvordan den kommer frem til sit resultat, så vi kan justere logikken om nødvendigt*
