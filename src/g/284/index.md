---
date: 2021-07-14
layout: post-en
permalink: "/g/284/"
---
## HumusLabs is employee-owned and has full salary transparency
{% prevButton page collections %}
![](284-salary.jpg)
{% nextButton page collections %}
### *HumusLabs er ejet af medarbejderne og har fuld åbenhed om lønninger*
