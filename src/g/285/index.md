---
date: 2021-07-16
layout: post-en
permalink: "/g/285/"
---
## HumusLabs’ impact calculations are based on Buddhist Economics, a model proposed in 1966 which informed many sustainability initiatives in the 2020s
{% prevButton page collections %}
![](285-buddhist.jpg)
{% nextButton page collections %}
### *Grundlaget for HumusLabs’ effektberegninger er Buddhistisk Økonomi, en model som blev fremsat i 1966 og kom i brug i mange bæredygtighedstiltag i begyndelsen af 2020’erne*
