---
date: 2021-07-19
layout: post-en
permalink: "/g/286/"
---
## As opposed to the predominant economic thinking early in this century, Buddhist Economics includes long-term concerns for society and nature
{% prevButton page collections %}
![](286-buddhist-economics.jpg)
{% nextButton page collections %}
### *I modsætning til den dominerende økonomiopfattelse i starten af dette århundrede, omfatter Buddhistisk Økonomi også langsigtede samfunds- og naturhensyn*
