---
date: 2021-07-21
layout: post-en
permalink: "/g/287/"
---
## We collect and promote local agricultural traditions from before the Middle Ages, each suitable for specific kinds of soil and climate
{% prevButton page collections %}
![](287-traditions.jpg)
{% nextButton page collections %}
### *Vi indsamler og formidler lokale landbrugstraditioner fra før middelalderen, hver egnet til specifikke jordtyper og klima*
