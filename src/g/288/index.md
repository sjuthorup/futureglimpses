---
date: 2021-07-23
layout: post-en
permalink: "/g/288/"
---
## Grandad is speaking at my birthday party, revisiting the speech he held at my birth about the green transformation he hoped for me to witness -- and quite a lot of it has come to pass
{% prevButton page collections %}
![](288-speech.jpg)
{% nextButton page collections %}
### *Farfar holder tale ved min fødselsdagsfest: Han læser højt af den tale, han holdt da jeg blev født, om den grønne omstilling han håbede at jeg ville blive vidne til -- og meget af det er gået i opfyldelse*
