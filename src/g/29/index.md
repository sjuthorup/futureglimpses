---
date: 2020-01-03
layout: post-en
permalink: "/g/29/"
---
## The Citizens’ Assembly this spring will consider marketing rules on the declaration of products’ environmental impact

{% prevButton page collections %}
![](29-green-appraisals.jpg)
{% nextButton page collections %}

### *Dette forårs borgerting skal tage stilling til markedsføringsreglerne for at deklarere produkters miljøpåvirkning*
