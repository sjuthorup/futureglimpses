---
date: 2019-10-13
layout: post-en
permalink: "/g/3/"
---

## One of my former classmates invented a type of battery that can hold 20% more power at the same weight

{% prevButton page collections %}
![](3-classmate-battery.jpg)
{% nextButton page collections %}

### *En af mine gamle skolekammerater har opfundet et batteri der kan kan rumme 20% mere energi med samme vægt*
