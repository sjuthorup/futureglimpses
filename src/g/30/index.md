---
date: 2020-01-05
layout: post-en
permalink: "/g/30/"
---
## I'm reading an exhilarating thriller about the dangerous work as a carbon controller in South America

{% prevButton page collections %}
![](30-thriller-carbon-controller.jpg)
{% nextButton page collections %}

### *Jeg er ved at læse en vildt spændende roman om det farlige arbejde som CO2-kontrollør i Sydamerika*
