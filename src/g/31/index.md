---
date: 2020-01-06
layout: post-en
permalink: "/g/31/"
---
## The desalination energy capture technology that the Gates Foundation made open source a few years ago has enabled most countries to accelerate their rewilding efforts

{% prevButton page collections %}
![](31-desalination.jpg)
{% nextButton page collections %}

### *Teknologien til energiproduktion fra afsaltning, som Gates-stiftelsen frigav rettighederne til for et par år siden, har sat næsten alle lande i stand til at øge deres naturgenskabelsesindsats drastisk*
