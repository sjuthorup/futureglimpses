---
date: 2020-01-07
layout: post-en
permalink: "/g/32/"
---
## The EU has finally removed all dispensation options for artificial pesticides in agriculture

{% prevButton page collections %}
![](32-removed-pesticide-dispensation.jpg)
{% nextButton page collections %}

### *Nu har EU endelig fjernet alle muligheder for dispensation til at bruge kunstige sprøjtemidler i landbruget*
