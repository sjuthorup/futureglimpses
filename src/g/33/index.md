---
date: 2020-01-10
layout: post-en
permalink: "/g/33/"
---
## Since the medical field of human gut flora was established, and science-based dietary advice and bacterial treatments became available, the pressure on public healthcare systems has eased up

{% prevButton page collections %}
![](33-microbiome.jpg)
{% nextButton page collections %}

### *Siden det medicinske speciale omkring tarmflora blev etableret, og der kom videnskabeligt baseret kostrådgivning og mulighed for bakteriebehandlinger, er presset på de offentlige sundhedssystemer aftaget*
