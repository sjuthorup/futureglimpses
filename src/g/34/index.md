---
date: 2020-01-12
layout: post-en
permalink: "/g/34/"
---
## The EU directive of public access to government officials’ salary information has meant a breakthrough upswing in citizens’ trust in governments

{% prevButton page collections %}
![](34-public-salary.jpg)
{% nextButton page collections %}

### *EU-direktivet om offentlig adgang til indkomst-oplysninger for offentligt ansatte har givet et kæmpe løft i borgernes tillid til regeringen*
