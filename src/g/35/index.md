---
date: 2020-01-13
layout: post-en
permalink: "/g/35/"
---
## The movement Transparency & Trust is working to extend the salary transparency principle to everyone, pointing to evidence that it will improve overall societal trust

{% prevButton page collections %}
![](35-open-books-trust.jpg)
{% nextButton page collections %}

### *Bevægelsen Transparens & Tillid arbejder for at udbrede princippet om indtægts-åbenhed til alle, med belæg i undersøgelser der viser at det vil øge den generelle tillid i samfundet*
