---
date: 2020-01-15
layout: post-en
permalink: "/g/36/"
---
## This week’s topic in our local middle school is the carbon cycle, and I am participating to describe how we monitor the regional carbon emissions and sequestration

{% prevButton page collections %}
![](36-school-carbon.jpg)
{% nextButton page collections %}

### *Denne uges emne i den lokale skole er kulstofkredsløbet, og jeg deltager for at fortælle om overvågningen af regionens CO2-udslip og optagelse*
