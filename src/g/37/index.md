---
date: 2020-01-18
layout: post-en
permalink: "/g/37/"
---
## At the student party last night, I met a designer of safety algorithms for self-driving cars. She explained how they continuously improve based on the data collected from cars on the road

{% prevButton page collections %}
![](37-student-party.jpg)
{% nextButton page collections %}

### *Til studenterfesten igår mødte jeg én, der designer sikkerheds-algoritmer til selvkørende biler. Hun forklarede hvordan de hele tiden forbedres ud fra den information de opsamler fra biler på gaden*
