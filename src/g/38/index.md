---
date: 2020-01-20
layout: post-en
permalink: "/g/38/"
---
## Now that 95% of vehicles are self-driving, the road fatality rates have fallen from 41 in the average European city to about 3

{% prevButton page collections %}
![](38-car-fatalities.jpg)
{% nextButton page collections %}

### *Nu hvor 95% af alle køretøjer er selvkørende, er tallene for trafikdrab faldet fra 41 i en gennemsnitlig europæisk storby til omkring 3*
