---
date: 2020-01-22
layout: post-en
permalink: "/g/39/"
---
## Because meat products are expensive due to climate taxes and animal welfare laws, all parts of the animals are eaten but in smaller amounts than before. Traditional Eastern European food culture is in vogue again

{% prevButton page collections %}
![](39-traditional-diet.jpg)
{% nextButton page collections %}

### *Fordi kød er dyrt på grund af klimaafgifter og dyreværnslovgivning, spiser man alle dele af landbrugsdyrene, men i mindre mængder end før. Traditionel østeuropæisk madkultur er blevet moderne igen*
