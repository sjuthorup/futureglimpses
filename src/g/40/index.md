---
date: 2020-01-24
layout: post-en
permalink: "/g/40/"
---
## The 12 largest original animals in Europe are now back in their natural habitats and their numbers are stable or growing

{% prevButton page collections %}
![](40-wild-animals.jpg)
{% nextButton page collections %}

### *Alle de 12 største oprindelige dyr i Europa er nu genetableret i deres naturlige habitater, og bestandene er stabile eller voksende*
