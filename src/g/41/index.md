---
date: 2020-01-27
layout: post-en
permalink: "/g/41/"
---
## At the first lesson of our Macro Ecology class I asked the other students to join me as volunteer insect counters this spring

{% prevButton page collections %}
![](41-class.jpg)
{% nextButton page collections %}

### *Ved den første forelæsning i Makroøkologi spurgte jeg de andre studerende, om de vil være med til det frivillige arbejde med insekt-tælling i løbet af foråret*
