---
date: 2020-01-29
layout: post-en
permalink: "/g/42/"
---
## A recent study calculated that decommissioning the least-used large motorways could double the number of public parks and recreational areas accessible to the average citizen

{% prevButton page collections %}
![](42-motorway-parks.jpg)
{% nextButton page collections %}

### *En nylig rapport viser at en afvikling af de mindst brugte store motorveje kunne fordoble antallet af parker og offentlige områder, som den enkelte borger har adgang til*
