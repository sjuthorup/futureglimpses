---
date: 2020-02-02
layout: post-en
permalink: "/g/43/"
---
## On our study tour to Brussels, among the sights we want to see is the Attenborough-Thunberg Memorial

{% prevButton page collections %}
![](43-attenborough-thunberg.jpg)
{% nextButton page collections %}

### *På vores studietur til Bruxelles vil vi bl.a se Attenborough-Thunberg mindesmærket*
