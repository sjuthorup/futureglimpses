---
date: 2020-02-04
layout: post-en
permalink: "/g/44/"
---
## Now that transportation is expensive, local businesses are thriving, people tend to meet in town and we feel connected to our local community

{% prevButton page collections %}
![](44-local-shops.jpg)
{% nextButton page collections %}

### *Nu da transport er dyrt, går det godt for de lokale butikker og producenter. Folk mødes på gaden, og vi føler os knyttet til lokalsamfundet*
