---
date: 2020-02-06
layout: post-en
permalink: "/g/45/"
---
## During the HoloAttend conference today it struck me that a few decades ago participants would have traveled halfway across the globe to meet

{% prevButton page collections %}
![](45-holo-attend.jpg)
{% nextButton page collections %}

### *Under HoloAttend-mødet idag slog det mig, at for nogle få årtier siden ville deltagerne have rejst den halve klode rundt for at mødes*
