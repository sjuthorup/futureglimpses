---
date: 2020-02-08
layout: post-en
permalink: "/g/46/"
---
## After a long campaign and international debates, universal anonymous access to free contraception was included in the Human Rights Declaration

{% prevButton page collections %}
![](46-free-contraception.jpg)
{% nextButton page collections %}

### *Efter en lang kampagne og internationale drøftelser blev Menneskerettighederne udvidet med retten til gratis og anonym adgang til prævention*
