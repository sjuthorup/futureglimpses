---
date: 2020-02-10
layout: post-en
permalink: "/g/47/"
---
## Now meat products are so expensive, often families share a dog or cat, so the individual pet brings joy to several people

{% prevButton page collections %}
![](47-share-dog.jpg)
{% nextButton page collections %}

### *Nu hvor kødprodukter er så dyre, deles familier tit om en hund eller kat, så mange mennesker har glæde af det enkelte kæledyr*
