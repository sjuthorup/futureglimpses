---
date: 2020-02-12
layout: post-en
permalink: "/g/48/"
---
## The European Commission confirms that the full ban on fossil fuels will enter into force in 2050 as planned

{% prevButton page collections %}
![](48-eu-fossil-ban.jpg)
{% nextButton page collections %}

### *EU-kommissionen bekræfter at det fuldstændige forbud mod fossile brændsler vil træde i kraft i 2050 som planlagt*
