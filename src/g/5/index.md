---
date: 2019-10-13
layout: post-en
permalink: "/g/5/"
---

## My aunt said: I just realized all of the ingredients for this meal were grown within walking distance

{% prevButton page collections %}
![](5-aunt-meal.jpg)
{% nextButton page collections %}

### *Min faster sagde: Det gik lige op for mig, at alle ingredienserne til aftensmaden er dyrket i gå-afstand herfra*
