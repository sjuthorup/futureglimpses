---
date: 2020-02-19
layout: post-en
permalink: "/g/50/"
---
## The European Commission now requires any lawmaker accepting meetings with industry lobbyists, to allocate the same amount of their time to corresponding NGOs

{% prevButton page collections %}
![](50-NGO-lobby.jpg)
{% nextButton page collections %}

### *EU-kommissionen kræver nu, at enhver lovgiver som holder møde med erhvervs-lobbyister, skal bruge samme mængde tid på at mødes med tilsvarende organisationer fra civilsamfundet*
