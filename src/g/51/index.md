---
date: 2020-02-21
layout: post-en
permalink: "/g/51/"
---
## The European Biodiversity Panel launched a new category of species categorization: Wandering -- in between Native and Invasive

{% prevButton page collections %}
![](51-wandering-species.jpg)
{% nextButton page collections %}

### *Det Europæiske Biodiversitetspanel vedtog en ny kategori af arter: Vandrende -- ind imellem Oprindelig og Invasiv*
