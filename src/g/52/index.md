---
date: 2020-02-23
layout: post-en
permalink: "/g/52/"
---
## The outdoor advertising ban entered fully into force, and I am noticing our city’s beautiful historic buildings, the birds and squirrels in the roadside trees, and my fellow citizens on the street

{% prevButton page collections %}
![](52-ban-outdoor-ads.jpg)
{% nextButton page collections %}

### *Efter udendørs reklamer er blevet helt forbudt, er jeg begyndt at lægge mærke til byens smukke historiske bygninger, dyrelivet i træerne langs vejen, og mine medborgere, de andre fodgængere*
