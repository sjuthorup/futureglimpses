---
date: 2020-02-25
layout: post-en
permalink: "/g/53/"
---
## Today’s big news is that parts of the Great Barrier Reef are rebounding with the new strands of heat-resistant corals

{% prevButton page collections %}
![](53-new-corals.jpg)
{% nextButton page collections %}

### *Dagens store nyhed er, at dele af Australiens Great Barrier Reef liver op igen ved hjælp af de nye varmeresistente typer koraller*
