---
date: 2020-02-27
layout: post-en
permalink: "/g/54/"
---
## Now that banks can only lend out money that they already borrowed, they compete on actual customer service, and no bank is too big to fail

{% prevButton page collections %}
![](54-bank-lend.jpg)
{% nextButton page collections %}

### *Nu da banker kun kan udlåne penge som allerede er indlånt, konkurrerer de på reel kundeservice, og ingen bank er “for fed til at fejle”*
