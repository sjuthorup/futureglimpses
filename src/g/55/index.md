---
date: 2020-02-29
layout: post-en
permalink: "/g/55/"
---
## The architectural movement Building at Human Scale arose at the same time climate taxes made steel and cement expensive. Now buildings are smaller, more welcoming, and more comfortable to live in than the old glass-and-concrete monsters

{% prevButton page collections %}
![](55-building-human-scale.jpg)
{% nextButton page collections %}

### *Arkitektur-trenden “Byg i menneskelig skala” opstod samtidig med at klimaskatterne gjorde stål og cement dyrere. Nutidens bygninger er mindre, mere indbydende, og mere behagelige at bo i, end de gamle glas-og-beton-monstre*
