---
date: 2020-03-03
layout: post-en
permalink: "/g/57/"
---
## Based on conclusive scientific evidence, the Consumer Protection Agency is prohibiting all plastics, regardless of fossil or vegetable origin, in toys for kids under the age of 5

{% prevButton page collections %}
![](57-plastic-toys.jpg)
{% nextButton page collections %}

### *På basis af videnskabelige beviser forbyder Forbrugerstyrelsen nu al plastik, uanset om det har fossil eller vegetabilsk oprindelse, i legetøj for børn under 5 år*
