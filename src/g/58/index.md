---
date: 2020-03-05
layout: post-en
permalink: "/g/58/"
---
## We’re watching an old film and laughing out loud: Smoke is pluming everywhere, from cars, houses, and people’s mouths

{% prevButton page collections %}
![](58-film-smoke.jpg)
{% nextButton page collections %}

### *Vi ser en gammel film og griner højt: Der kommer røg ud overalt, fra biler og huse og ud af munden på folk*
