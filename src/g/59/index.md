---
date: 2020-03-07
layout: post-en
permalink: "/g/59/"
---
## The trend “Farming at Human Scale” is spreading. Data collection and knowledge sharing enable many people to farm part-time, and health is improving for animals and people alike

{% prevButton page collections %}
![](59-part-time-farmer.jpg)
{% nextButton page collections %}

### *Trenden “Landbrug i menneskelig skala” breder sig. Dataopsamling og videndeling gør mange i stand til at være deltidslandmænd, og det giver bedre helbred for både dyr og mennesker*
