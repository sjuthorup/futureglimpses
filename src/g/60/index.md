---
date: 2020-03-09
layout: post-en
permalink: "/g/60/"
---
## On my way to visiting my friend in Lisbon, waking up well rested on the high-speed night train

{% prevButton page collections %}
![](60-night-train.jpg)
{% nextButton page collections %}

### *Jeg skal besøge min ven i Lissabon og har sovet skønt i sovevognen i lyntoget*
