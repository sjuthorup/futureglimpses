---
date: 2020-03-11
layout: post-en
permalink: "/g/61/"
---
## The Intergovernmental Science Board on Biodiversity announced that the number of critically endangered species is now decreasing for the first time

{% prevButton page collections %}
![](61-endangered-decreasing.jpg)
{% nextButton page collections %}

### *FN’s biodiversitetsråd bekendtgjorde at antallet af udryddelsestruede arter nu for første gang er faldende*
