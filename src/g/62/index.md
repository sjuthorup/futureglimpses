---
date: 2020-03-13
layout: post-en
permalink: "/g/62/"
---
## I received a status update from the Health Research Agency thanking me for my contribution of anonymous health data which has helped the development of several new diagnostic tools

{% prevButton page collections %}
![](62-health-data-thanks.jpg)
{% nextButton page collections %}

### *Jeg fik en statusinformation fra Styrelsen for Sundhedsforskning, som takkede for brugen af mine anonymiserede helbredsdata, der har bidraget til udvikling af en række nye diagnoseværktøjer*
