---
date: 2020-03-15
layout: post-en
permalink: "/g/63/"
---
## Since the EU’s adoption of the Happiness Index as its main performance indicator instead of GDP, many other things have improved: mental health, volunteering, less crime, use of public libraries and museums

{% prevButton page collections %}
![](63-art-museum.jpg)
{% nextButton page collections %}

### *Siden EU begyndte at bruge Lykkeindekset som primært resultatmål i stedet for BNP, er mange ting forbedret: mental sundhed, frivilligt arbejde, lavere kriminalitet, brug af biblioteker og museer*
