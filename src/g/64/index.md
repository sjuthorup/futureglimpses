---
date: 2020-03-17
layout: post-en
permalink: "/g/64/"
---
## These past few weeks when we’ve had so little wind, everyone sets their washing machine to run at night

{% prevButton page collections %}
![](64-washing-nights.jpg)
{% nextButton page collections %}

### *De seneste uger hvor vi har haft så lidt vind, har alle sat vaskemaskinen til at køre om natten*
