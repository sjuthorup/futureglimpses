---
date: 2020-03-21
layout: post-en
permalink: "/g/66/"
---
## All teachers have basic competences in natural resources and democracy, and children learn holistic thinking from an early age

{% prevButton page collections %}
![](66-holistic-thinking.jpg)
{% nextButton page collections %}

### *Alle lærere har nu en grunduddannelse i naturressourcer og demokrati, og børn lærer tidligt at tænke helhedsorienteret*
