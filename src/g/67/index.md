---
date: 2020-03-23
layout: post-en
permalink: "/g/67/"
---
## In the 22 European countries that mandate full salary transparency, men’s and women’s pay are now equal

{% prevButton page collections %}
![](67-equal-pay.jpg)
{% nextButton page collections %}

### *I de 22 europæiske lande der har krav om fuld åbenhed om lønoplysninger, er der nu ligeløn mellem mænd og kvinder*
