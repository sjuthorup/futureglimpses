---
date: 2020-03-25
layout: post-en
permalink: "/g/68/"
---
## 3D-printing of spare parts enables remote villages to enjoy modern technology

{% prevButton page collections %}
![](68-remote-village.jpg)
{% nextButton page collections %}

### *3D-print af reservedele gør det muligt for små landsbyer på fjerne steder at få nytte af ny teknologi*
