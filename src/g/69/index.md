---
date: 2020-03-26
layout: post-en
permalink: "/g/69/"
---
## The standard work week is 30 hours, many people work just 20 hours, and a few choose a 35 or 40 hour week

{% prevButton page collections %}
![](69-feed-ducks.jpg)
{% nextButton page collections %}

### *Den normale arbejdsuge er 30 timer, mange arbejder bare 20 timer, og nogle få vælger 35 eller 40 timers arbejdsuge*
