---
date: 2019-11-29
layout: post-en
permalink: "/g/7/"
---

## The UN Conference on Population celebrated that the size of the human population has stabilized

{% prevButton page collections %}
![](7-UN-population-stable.jpg)
{% nextButton page collections %}

### *FN's konference om befolkning markerede, at verdens befolkningtal nu ikke længere er stigende*
