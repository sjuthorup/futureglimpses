---
date: 2020-03-28
layout: post-en
permalink: "/g/70/"
---
## The curtailment of patent rights has brought tech and pharma companies’ profits down to the level of normal company profits

{% prevButton page collections %}
![](70-curtail-patents.jpg)
{% nextButton page collections %}

### *Begrænsningen af patent-rettigheder har nedbragt overskuddet hos teknologi- og medicinalfirmaer ned på niveau med normale virksomhedsoverskud*
