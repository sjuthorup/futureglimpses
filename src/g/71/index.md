---
date: 2020-03-29
layout: post-en
permalink: "/g/71/"
---
## All over Europe the ground water is so clean that no treatment is necessary

{% prevButton page collections %}
![](71-clean-water.jpg)
{% nextButton page collections %}

### *I hele Europa er grundvandet nu så rent, at det ikke behøver nogen rensning*
