---
date: 2020-03-31
layout: post-en
permalink: "/g/72/"
---
## Children learn Euro-English from first grade, and all schools have friendship schools in other EU countries, creating pan-European understanding and spirit

{% prevButton page collections %}
![](72-euro-english.jpg)
{% nextButton page collections %}

### *Børnene lærer euro-engelsk fra første klasse, og alle skoler har venskabs-skoler i andre EU-lande, hvilket skaber tvær-europæisk forståelse og ånd*
