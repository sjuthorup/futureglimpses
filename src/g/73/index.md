---
date: 2020-04-02
layout: post-en
permalink: "/g/73/"
---
## In general, food must contain a maximum of 5 ingredients

{% prevButton page collections %}
![](73-five-ingredients.jpg)
{% nextButton page collections %}

### *Fødevarer må normalt højst have 5 ingredienser*
