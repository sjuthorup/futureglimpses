---
date: 2020-04-03
layout: post-en
permalink: "/g/74/"
---
## There are no public holidays for religious reasons, but everybody has an allowance of 5 days off they can use anytime

{% prevButton page collections %}
![](74-no-holy-days.jpg)
{% nextButton page collections %}

### *Der er ingen officielle helligdage, men alle har et klippekort på 5 dages fri, som de kan tage når de vil*
