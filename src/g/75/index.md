---
date: 2020-04-05
layout: post-en
permalink: "/g/75/"
---
## As population numbers continue their slow decline, governments start rewilding of city and suburban districts

{% prevButton page collections %}
![](75-rewilding-districts.jpg)
{% nextButton page collections %}

### *I takt med at befolkningstallet langsomt falder, går regeringerne igang med naturgenskabelse i byer og forstæder*
