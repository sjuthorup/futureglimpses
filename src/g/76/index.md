---
date: 2020-04-06
layout: post-en
permalink: "/g/76/"
---
## The EU comprises 39 countries with a common minimum wage, basic pension and health care system

{% prevButton page collections %}
![](76-health-pensions.jpg)
{% nextButton page collections %}

### *EU består af 39 lande med en fælles mindsteløn, folkepension og sundhedssystem*
