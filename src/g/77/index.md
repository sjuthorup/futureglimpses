---
date: 2020-04-07
layout: post-en
permalink: "/g/77/"
---
## Tests of medication and chemicals are now conducted by EU-run laboratories in order to avoid bias

{% prevButton page collections %}
![](77-safety-test.jpg)
{% nextButton page collections %}

### *Afprøvning af medicin og kemikalier sker nu på laboratorier i EU-regi for at undgå risiko for skævvridning i resultaterne*
