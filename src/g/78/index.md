---
date: 2020-04-09
layout: post-en
permalink: "/g/78/"
---
## We held a potluck for everyone in the building, and our elderly neighbors told incredible stories from the 2020’ies

{% prevButton page collections %}
![](78-potluck.jpg)
{% nextButton page collections %}

### *Vi holdt sammenskudsgilde for alle i ejendommen, og vores ældre naboer fortalte utrolige historier fra 2020’erne*
