---
date: 2020-04-10
layout: post-en
permalink: "/g/79/"
---
## The former coal mine near Katowice has been transformed to a nature exploratorium of endangered bird and amphibian species

{% prevButton page collections %}
![](79-coal-mine.jpg)
{% nextButton page collections %}

### *Den tidligere kulmine nær Katowice er blevet forvandlet til et natur-eksperimentarium for truede arter af fugle og padder*
