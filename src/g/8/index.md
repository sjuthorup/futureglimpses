---
date: 2019-11-30
layout: post-en
permalink: "/g/8/"
---

## The EU instigated a donor scheme with all African countries to build battery and wind turbine factories in return for their phasing out fossil fuel usage

{% prevButton page collections %}
![](8-wind-manufacturing.jpg)
{% nextButton page collections %}

### *EU etablerede en bistandsmodel for alle afrikanske lande om at bygge batteri- og vindmøllefabrikker til gengæld for at de udfaser fossile brændsler*
