---
date: 2020-04-11
layout: post-en
permalink: "/g/80/"
---
## Obsolete oil production platforms in the North Sea have become artificial reefs where the ocean fauna thrives

{% prevButton page collections %}
![](80-oil-platform.jpg)
{% nextButton page collections %}

### *Udtjente boreplatforme i Nordsøen er blevet kunstige rev, hvor dyrelivet trives*
