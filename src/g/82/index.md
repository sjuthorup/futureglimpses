---
date: 2020-04-14
layout: post-en
permalink: "/g/82/"
---
## The underground tanks of former petrol stations are being reused for Compressed Air Storage to provide flexibility in the energy system

{% prevButton page collections %}
![](82-petrol-station.jpg)
{% nextButton page collections %}

### *Tankene under tidligere benzinstationer bliver genbrugt til trykluft-lager, som giver fleksibilitet i energisystemet*
