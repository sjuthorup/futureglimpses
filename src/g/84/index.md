---
date: 2020-04-16
layout: post-en
permalink: "/g/84/"
---
## Fitness centres have outdoor exercise areas supplementing their indoor areas

{% prevButton page collections %}
![](84-fitness.jpg)
{% nextButton page collections %}

### *Fitnesscentre har udendørs træningsområder som supplement til deres lokaler*
