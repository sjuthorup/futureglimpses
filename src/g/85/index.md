---
date: 2020-04-17
layout: post-en
permalink: "/g/85/"
---
## Stumbled upon a job site, my grandad is reading aloud, marveling at job roles that have emerged since his youth: drone guide, microbiomist, artificial intelligence trainer, health detective, biosociologist, holoarchitect...

{% prevButton page collections %}
![](85-grandad-job-site.jpg)
{% nextButton page collections %}

### *Min farfar er faldet over et job-site og læser fascineret højt af de jobtitler der er opstået siden hans ungdom: droneguide, mikrobiomiker, kunstig-intelligens-træner, sundhedsdetektiv, biosociolog, holoarkitekt...*
