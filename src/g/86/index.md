---
date: 2020-04-18
layout: post-en
permalink: "/g/86/"
---
## Recycling of household waste is 98% on average and contributes well to the municipality’s income

{% prevButton page collections %}
![](86-recycling.jpg)
{% nextButton page collections %}

### *Gennemsnitligt genbruges 98% af husholdningsaffaldet, og det giver kommunen en god indtægt*
