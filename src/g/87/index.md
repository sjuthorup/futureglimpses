---
date: 2020-04-19
layout: post-en
permalink: "/g/87/"
---
## Every neighborhood has a sharing arrangement for tools and other items that everyone uses only once in a while

{% prevButton page collections %}
![](87-share-tools.jpg)
{% nextButton page collections %}

### *Ethvert lokalområde har en fælles ordning for værktøj og andre ting som man bruger sjældent*
