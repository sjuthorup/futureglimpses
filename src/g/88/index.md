---
date: 2020-04-21
layout: post-en
permalink: "/g/88/"
---
## In class today we learned how peak oil, peak “stuff” and the peak of clinical stress happened one after the other during a few years

{% prevButton page collections %}
![](88-peak-stuff.jpg)
{% nextButton page collections %}

### *I undervisningen idag lærte vi, hvordan olieproduktionen, forbruget af ting, og antal stresstilfælde toppede med få års mellemrum*
