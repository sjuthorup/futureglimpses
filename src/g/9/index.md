---
date: 2019-12-01
layout: post-en
permalink: "/g/9/"
---
## My mom was going through my late grandma’s stuff and found an actual paper newspaper from 2019 that made her cry all afternoon

{% prevButton page collections %}
![](9-mother-cry.jpg)
{% nextButton page collections %}

### *Efter min mormors død gennemgik min mor hendes ting og fandt en papir-avis fra 2019, som fik hende til at græde hele eftermiddagen*
