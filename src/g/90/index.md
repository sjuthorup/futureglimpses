---
date: 2020-04-23
layout: post-en
permalink: "/g/90/"
---
## As a part of their Society curriculum, my cousin’s school class occasionally helps out at a farm, a furniture factory and a recycling plant

{% prevButton page collections %}
![](90-class-recycling.jpg)
{% nextButton page collections %}

### *Som led i deres samfundsundervisning hjælper min kusines klasse nogle gange til på en gård, en møbelfabrik og en genbrugsstation*
