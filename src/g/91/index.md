---
date: 2020-04-25
layout: post-en
permalink: "/g/91/"
---
## To simplify the legislation, the parliament now works to eliminate 2 existing laws every time they make a new one

{% prevButton page collections %}
![](91-eliminate-laws.jpg)
{% nextButton page collections %}

### *For at forenkle lovgivningen bestræber parlamentet sig på at fjerne 2 eksisterende love, hver gang de laver en ny*
