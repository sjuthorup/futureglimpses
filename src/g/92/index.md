---
date: 2020-04-27
layout: post-en
permalink: "/g/92/"
---
## My dad is on this year’s Citizens’ Advisory Board, and he often calls me to discuss the law proposals before he votes 

{% prevButton page collections %}
![](92-dad-advisory.jpg)
{% nextButton page collections %}

### *Min far er udpeget til dette års rådgivende borgerpanel, og han ringer tit til mig for at snakke om hvad han skal stemme*
