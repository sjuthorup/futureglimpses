---
date: 2020-04-29
layout: post-en
permalink: "/g/94/"
---
## Even when stationed at the energy island, my brother rehearses with his band 

{% prevButton page collections %}
![](94-brother-rehearse.jpg)
{% nextButton page collections %}

### *Selv når han er udstationeret på energi-øen, øver min bror sammen med sit band*
