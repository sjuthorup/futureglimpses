---
date: 2020-05-01
layout: post-en
permalink: "/g/95/"
---
## Construction began on the Hyperrail network that will make it possible to travel in less than 12 hours from any European capital to any other 

{% prevButton page collections %}
![](95-hyperrail.jpg)
{% nextButton page collections %}

### *Anlægsarbejdet er begyndt på Hyperrail-nettet, der skal muliggøre rejser på mindre end 12 timer fra en hvilken som helst europæisk hovedstad til en anden*
