---
date: 2020-05-03
layout: post-en
permalink: "/g/97/"
---
## Europe’s old port cities are trying out different ways of dealing with the rising sea level 

{% prevButton page collections %}
![](97-dige.jpg)
{% nextButton page collections %}

### *Europas gamle havnebyer afprøver forskellige måder at håndtere det stigende havniveau*
