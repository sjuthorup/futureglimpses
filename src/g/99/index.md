---
date: 2020-05-05
layout: post-en
permalink: "/g/99/"
---
## Waste fabrics and old clothes are recycled -- cotton and linen make a nice quality of rayon

{% prevButton page collections %}
![](99-recycled-clothes.jpg)
{% nextButton page collections %}

### *Stofrester og gammelt tøj indsamles og forarbejdes til nyt stof -- bomuld og hør bliver en lækker viscose-kvalitet*
