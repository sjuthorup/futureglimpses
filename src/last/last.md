<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="noindex" />
  <script type="text/javascript">
    var origin = window.location.origin;
    {%- for post in collections.postRecent limit:1 -%}
    var lastGlimpseNumber = parseInt('{{ post.data.permalink }}'.match(/\/g\/(\d+)/)[1]);
    {%- endfor -%}
    window.location.href = origin + "/g/" + lastGlimpseNumber;
  </script>
</head>
<body>
</body>
